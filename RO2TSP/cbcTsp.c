#include "cbcTsp.h"

void cbc_build_model_base(instance *inst, Cbc_Model *mod) {

    // VARIABLES DEFINITION
    const double lower_bound_x = 0.0;                    // Lower bound of the variables
    const double upper_bound_x = 1.0;                    // Upper bound of the variables
    char integral = '1';
    char *cname = (char *) calloc(100, sizeof(char));    // Allocation of the single array element

    // Create all the variables cur_x(j,i) corresponding to the edges from the node j to i
    // ingnoring self loops and considering the edges as not oriented
    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = i + 1; j < inst->nnodes; j++) {

            sprintf(cname, "x(%d,%d)", i+1, j+1);    // +1 index shift to start counting nodes from one
            double cost = dist(inst, i, j);                // The cost is the distance between the nodes

            Cbc_addCol(mod, cname, lower_bound_x, upper_bound_x, cost, integral, 0, NULL, NULL);

            if (Cbc_getNumCols(mod) - 1 != cplex_pos_base(inst, i, j)) print_error(" wrong position for x var.s");
        }
    }

    // CONSTRAINTS DEFINITION
    const double b = 2.0;
    const char sense = 'E';
    int nz = inst->nnodes - 1;
    int nz_index = 0;
    int *cols = (int *) calloc(nz, sizeof(int));
    double *coefs = (double *) calloc(nz, sizeof(double));

    // For each vertex v
    for (int v = 0; v < inst->nnodes; v++) {

        sprintf(cname, "vertice(%d)", v + 1);

        // For each vertex u different from v
        nz_index = 0;
        for (int u = 0; u < inst->nnodes; u++) {
            if (u == v) continue;
            coefs[nz_index] = 1.0;
            cols[nz_index++] = cplex_pos_base(inst, u, v);
        }

        Cbc_addRow(mod, cname, nz, cols, coefs, sense, b);
    }

    free(cols);
    free(coefs);
    free(cname);
}

void cbc_build_model_compact_MTZ(instance *inst, Cbc_Model *mod) {

    // VARIABLES DEFINITION
    const double lower_bound_y = 0.0;                    // Lower bound of the variables
    const double upper_bound_y = 1.0;                    // Upper bound of the variables
    const char integral = '1';
    char *cname = (char *) calloc(100, sizeof(char));    // Allocation of the single array element

    // Create all the variables y(j,i) corresponding to the edges from the node j to i
    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = 0; j < inst->nnodes; j++) {

            sprintf(cname, "y(%d,%d)", i + 1, j + 1);    // +1 index shift to start counting nodes from one
            double cost = dist(inst, i, j);                    // The cost is the distance between the nodes

            Cbc_addCol(mod, cname, lower_bound_y, ((i == j) ? lower_bound_y : upper_bound_y), cost, integral, 0, NULL,
                       NULL);
            if (Cbc_getNumCols(mod) - 1 != cplex_pos_compact_MTZ(inst, i, j))
                print_error(" wrong position for y var.s");
        }
    }

    const double lower_bound_u = 0.0;
    const double upper_bound_u = inst->nnodes - 1;
    const double cost = 0;

    // Create all variables u(j) corriesponding to the position of the j-th node in the circuit
    for (int i = 0; i < inst->nnodes; i++) {
        sprintf(cname, "u(%d)", i + 1);
        Cbc_addCol(mod, cname, lower_bound_u, ((i == 0) ? lower_bound_u : upper_bound_u), cost, integral, 0, NULL,
                   NULL);
    }

    // CONSTRAINTS DEFINITION

    // Static Constraints
    const double b = 1.0;
    const char sense = 'E';
    int nz = inst->nnodes - 1;
    int nz_index = 0;
    int *cols = (int *) calloc(nz, sizeof(int));
    double *coefs = (double *) calloc(nz, sizeof(double));

    // For each vertex v
    for (int v = 0; v < inst->nnodes; v++) {

        sprintf(cname, "vertice(%d)_archi_entranti", v + 1);

        // For each vertex u different from v
        nz_index = 0;
        for (int u = 0; u < inst->nnodes; u++) {
            if (u == v) continue;
            cols[nz_index] = cplex_pos_compact_MTZ(inst, u, v);
            coefs[nz_index++] = 1.0;
        }

        Cbc_addRow(mod, cname, nz, cols, coefs, sense, b);

        sprintf(cname, "vertice(%d)_archi_uscenti", v + 1);

        nz_index = 0;
        for (int u = 0; u < inst->nnodes; u++) {
            if (u == v) continue;
            cols[nz_index++] = cplex_pos_compact_MTZ(inst, v, u);
        }

        Cbc_addRow(mod, cname, nz, cols, coefs, sense, b);
    }

    // Lazy Constraints - Not yet supported in CBC, using static

    // y(j,i) + y(i,j) <= 1
    int nzcnt = 2;
    double rhs = 1.0;
    const char sense_l = 'L';
    int rmatind[2];
    double rmatval[2] = {1.0, 1.0};

    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = i + 1; j < inst->nnodes; j++) {
            rmatind[0] = cplex_pos_compact_MTZ(inst, i, j);
            rmatind[1] = cplex_pos_compact_MTZ(inst, j, i);
            sprintf(cname, "lazy_y(%d,%d)y(%d,%d)", i, j, j, i);
            Cbc_addRow(mod, cname, nzcnt, rmatind, rmatval, sense_l, rhs);
        }
    }

    // u(j) - u(i) - bigM*y(i,j) >= 1 - bigM
    nzcnt = 3;
    const int bigM = inst->nnodes - 1;
    rhs = 1 - bigM;
    const char sense_g = 'G';
    int rmatindU[3];
    double rmatvalU[3] = {1.0, -1.0, -bigM};
    int offset = (int) pow(inst->nnodes, 2);    // to skip the y(j,i) variables

    for (int j = offset + 1; j < offset + inst->nnodes; j++) {
        for (int i = offset; i < offset + inst->nnodes; i++) {
            if (j == i) continue;
            rmatindU[0] = j;
            rmatindU[1] = i;
            rmatindU[2] = cplex_pos_compact_MTZ(inst, i - offset, j - offset);
            sprintf(cname, "lazy_u(%d,%d)", j - offset, i - offset);
            Cbc_addRow(mod, cname, nzcnt, rmatindU, rmatvalU, sense_g, rhs);
        }
    }

    free(cname);
    free(cols);
    free(coefs);
}

void cbc_build_model_compact_custom(instance *inst, Cbc_Model *mod) {}

void cbc_build_model_compact_F1(instance *inst, Cbc_Model *mod) {


    // VARIABLES DEFINITION
    const double lower_bound_x = 0.0;                    // Lower bound of the variables
    const double upper_bound_x = 1.0;                    // Upper bound of the variables
    const char integral = '1';
    char *cname = (char *) calloc(100, sizeof(char));        // Allocation of the single array element

    // Create all the variables cur_x(j,i) corresponding to the edges from the node j to i
    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = 0; j < inst->nnodes; j++) {

            sprintf(cname, "x(%d,%d)", i + 1, j + 1);    // +1 index shift to start counting nodes from one
            double cost = dist(inst, i, j);                    // The cost is the distance between the nodes

            Cbc_addCol(mod, cname, lower_bound_x, ((i == j) ? lower_bound_x : upper_bound_x), cost, integral, 0, NULL,
                       NULL);
            if (Cbc_getNumCols(mod) - 1 != cplex_pos_compact_MTZ(inst, i, j))
                print_error(" wrong position for x var.s");
        }
    }

    const double lower_bound_y = 0.0;
    const double upper_bound_y = inst->nnodes - 1;
    const char continuous = '0';
    const double cost = 0;    // The cost is the distance between the nodes

    // Create all the variables y(j,i) corresponding to the flow from the node j to i
    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = 0; j < inst->nnodes; j++) {

            sprintf(cname, "y(%d,%d)", i + 1, j + 1);    // +1 index shift to start counting nodes from one

            Cbc_addCol(mod, cname, lower_bound_y, ((i == j) ? lower_bound_y : upper_bound_y), cost, continuous, 0, NULL,
                       NULL);
            if (Cbc_getNumCols(mod) - 1 != cplex_pos_compact_F1(inst, i, j)) print_error(" wrong position for x var.s");
        }
    }

    // CONSTRAINTS DEFINITION

    // Static Constraints
    const double b = 1.0;
    const char sense_e = 'E';
    int nz = inst->nnodes - 1;
    int nz_index = 0;
    int *cols = (int *) calloc(2 * nz, sizeof(int));
    double *coefs = (double *) calloc(2 * nz, sizeof(double));

    // For each vertex v
    for (int v = 0; v < inst->nnodes; v++) {

        sprintf(cname, "vertice(%d)_archi_entranti", v + 1);

        // For each vertex u different from v
        nz_index = 0;
        for (int u = 0; u < inst->nnodes; u++) {
            if (u == v) continue;
            cols[nz_index] = cplex_pos_compact_MTZ(inst, u, v);
            coefs[nz_index++] = 1.0;
        }

        Cbc_addRow(mod, cname, nz, cols, coefs, sense_e, b);

        sprintf(cname, "vertice(%d)_archi_uscenti", v + 1);

        nz_index = 0;
        for (int u = 0; u < inst->nnodes; u++) {
            if (u == v) continue;
            cols[nz_index++] = cplex_pos_compact_MTZ(inst, v, u);
        }

        Cbc_addRow(mod, cname, nz, cols, coefs, sense_e, b);
    }

    // Constraints on maxflow on an edge
    const char sense_l = 'L';
    const double z = 0.0;
    nz = 2;

    for (int i = 0; i < inst->nnodes; i++) {
        for (int j = 0; j < inst->nnodes; j++) {
            if (i == j) continue;
            sprintf(cname, "arco(%d,%d)_massimo_flusso", i + 1, j + 1);
            cols[0] = cplex_pos_compact_F1(inst, i, j);
            coefs[0] = 1.0;
            cols[1] = cplex_pos_compact_MTZ(inst, i, j);
            coefs[1] = (i == 0 || j == 0) ? -inst->nnodes + 1 : -inst->nnodes + 2;
            Cbc_addRow(mod, cname, nz, cols, coefs, sense_l, z);
        }
    }

    // Constraints on maxflow out of edge 1
    const double t = inst->nnodes - 1;
    nz = inst->nnodes - 1;

    sprintf(cname, "somma_flussi_uscenti_dal_nodo_1");
    for (int j = 1; j < inst->nnodes; j++) {
        cols[j - 1] = cplex_pos_compact_F1(inst, 0, j);
        coefs[j - 1] = 1.0;
    }
    Cbc_addRow(mod, cname, nz, cols, coefs, sense_e, t);

    // Constraints on income and outcome flow
    nz = 2 * (inst->nnodes - 1);

    for (int j = 1; j < inst->nnodes; j++) {
        sprintf(cname, "vertice(%d)_flusso_entrante_uscente", j + 1);
        nz_index = 0;

        for (int i = 0; i < inst->nnodes; i++) {    // Archi entranti
            if (i == j) continue;
            cols[nz_index] = cplex_pos_compact_F1(inst, i, j);
            coefs[nz_index++] = 1.0;
        }
        for (int i = 0; i < inst->nnodes; i++) {    // Archi uscenti
            if (i == j) continue;
            cols[nz_index] = cplex_pos_compact_F1(inst, j, i);
            coefs[nz_index++] = -1.0;
        }
        Cbc_addRow(mod, cname, nz, cols, coefs, sense_e, b);
    }

    free(cname);
}

int cbc_solve_model(instance *inst, Cbc_Model **mod) {
   switch (inst->model) {
        case MODEL_BASE:
        case MODEL_BASE_LOOP:
            return cbc_loop(inst, mod);
            break;
        case MODEL_BASE_CALLBACK_LOOP:
            return cbc_cloop(inst, mod);
        case MODEL_BASE_HARD_FIXING:
            //return hcloop(inst, env, lp);
        case MODEL_BASE_LOCAL_BRANCHING:
            //return localbranching(inst, env, lp);
            break;
    }
    return Cbc_solve(*mod);
}

int cbc_exec_solver(instance *inst, Cbc_Model **mod) {

    switch (inst->model) {
        case MODEL_BASE:
        case MODEL_BASE_LOOP:
        case MODEL_BASE_CALLBACK_LOOP:
        case MODEL_BASE_HARD_FIXING:
        case MODEL_BASE_LOCAL_BRANCHING:
            cbc_build_model_base(inst, *mod);                    // Build the model
            //Cbc_writeLp(*mod, "cbc_TSP_model_base");
            break;
        case MODEL_COMPACT_MTZ:
            cbc_build_model_compact_MTZ(inst, *mod);
            //Cbc_writeLp(*mod, "cbc_TSP_model_compact_MTZ");
            break;
        case MODEL_COMPACT_CUSTOM:
            cbc_build_model_compact_custom(inst, *mod);
            //Cbc_writeLp(*mod, "cbc_TSP_model_compact_custom");
            break;
        case MODEL_COMPACT_F1:
            cbc_build_model_compact_F1(inst, *mod);
            //Cbc_writeLp(*mod, "cbc_TSP_model_compact_F1");
            break;
    }

    if (inst->solve) return cbc_solve_model(inst, mod);
    return 0;
}

void cbc_add_constraint_loop(instance *inst, Cbc_Model *mod, const int *comp){

    int ncuts = 0;  // # of cuts = # of subtours (components)
    char *cname = (char *) calloc(100, sizeof(char));
    const char sense_l = 'L';
    // For each component it tracks the number of non-zero elements in the corresponding constraint
    // (all the edges in the subtour) (Length = nnodes)
    int* nzcnt = (int*)calloc(inst->nnodes, sizeof(int));
    // For each component it tracks |S|-1, the right-hand side of the constraint
    int* rhs = (int*)calloc(inst->nnodes, sizeof(int));
    // For each component it initializes a pointer to the indices of the non-zero elements of the constraint.
    // If a component does not represent a subtour the inmost array is not allocated
    int** cutind = (int**)calloc(inst->nnodes, sizeof(int*));
    // Same as cutind but for the coefficients of the constraints
    double** cutval = (double**)calloc(inst->nnodes, sizeof(double*));

    for (int i = 0; i < inst->nnodes; i++) {
        if (nzcnt[comp[i]] == 0) {  // Then initialize constraint for this subtour
            ncuts++;
            for (int j = i + 1; j < inst->nnodes; j++)
                if(comp[i] == comp[j])
                    rhs[comp[i]]++; // Increment right-hand side
            cutind[comp[i]] = (int*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(int));
            cutval[comp[i]] = (double*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(double));
        }
        for (int j = i + 1; j < inst->nnodes; j++) {
            if (comp[i] == comp[j]) {   // Then update the coefficients in the corresponding cutind and cutval constraints
                cutind[comp[i]][nzcnt[comp[i]]] = cplex_pos_base(inst, i, j);
                cutval[comp[i]][nzcnt[comp[i]]++] = 1.0;
            }
        }
    }

    for (int i = 0; i < inst->nnodes; i++) { // Add the cuts
        if (nzcnt[i] == 0) continue; // If component was empty continue
        sprintf(cname, "subtour_constraint_%d", Cbc_getNumRows(mod) - inst->nnodes + 1);
        Cbc_addRow(mod, cname, nzcnt[i], cutind[i], cutval[i], sense_l, rhs[i]);
    }

    free(*cutval);	free(cutval);
    free(*cutind);	free(cutind);
    free(rhs);
    free(nzcnt);
    free(cname);
}

int cbc_add_constraint_callback_cloop(instance *inst, void *osiCuts, void *osiSolver, const int *comp){

    int ncuts = 0;  // # of cuts = # of subtours (components)
    const char sense_l = 'L';
    // For each component it tracks the number of non-zero elements in the corresponding constraint
    // (all the edges in the subtour) (Length = nnodes)
    int* nzcnt = (int*)calloc(inst->nnodes, sizeof(int));
    // For each component it tracks |S|-1, the right-hand side of the constraint
    int* rhs = (int*)calloc(inst->nnodes, sizeof(int));
    // For each component it initializes a pointer to the indices of the non-zero elements of the constraint.
    // If a component does not represent a subtour the inmost array is not allocated
    int** cutind = (int**)calloc(inst->nnodes, sizeof(int*));
    // Same as cutind but for the coefficients of the constraints
    double** cutval = (double**)calloc(inst->nnodes, sizeof(double*));

    for (int i = 0; i < inst->nnodes; i++) {
        if (nzcnt[comp[i]] == 0) {  // Then initialize constraint for this subtour
            ncuts++;
            for (int j = i + 1; j < inst->nnodes; j++)
                if(comp[i] == comp[j])
                    rhs[comp[i]]++; // Increment right-hand side
            cutind[comp[i]] = (int*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(int));
            cutval[comp[i]] = (double*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(double));
        }
        for (int j = i + 1; j < inst->nnodes; j++) {
            if (comp[i] == comp[j]) {   // Then update the coefficients in the corresponding cutind and cutval constraints
                cutind[comp[i]][nzcnt[comp[i]]] = cplex_pos_base(inst, i, j);
                cutval[comp[i]][nzcnt[comp[i]]++] = 1.0;
            }
        }
    }

    int nz = 0;
    for (int i = 0; i < inst->nnodes; i++) { // Add the cuts
        if (nzcnt[i] == 0) continue; // If component was empty continue
        OsiCuts_addRowCut(osiCuts, nzcnt[i], cutind[i], cutval[i], sense_l, rhs[i]);
        nz++;
    }

    free(*cutval);	free(cutval);
    free(*cutind);	free(cutind);
    free(rhs);
    free(nzcnt);
    return nz;
}

void cbc_add_constraint_cbc_cloop(void *osiSolver, void *osiCuts, void *appdata){
    instance* inst = (instance *)appdata;
    const double* x_curr = Osi_getColSolution(osiSolver);

    int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    set_components(inst, comp, x_curr);
    int ncuts = 0;
    if(has_loop(inst, comp)) {
        ncuts = cbc_add_constraint_callback_cloop(inst, osiCuts, osiSolver, comp);
    }

    free(comp);
}


int cbc_cloop(instance* inst, Cbc_Model **mod){
    Cbc_addCutCallback(*mod, cbc_add_constraint_cbc_cloop, "CUT_CALLBACK", inst, 0, 1); // 0 stands for howOften, 1 is a true boolean value. It means that the callback must be called every time there is an integer solution
    int res = Cbc_solve(*mod);
    return res;
}

int cbc_loop(instance *inst, Cbc_Model **mod) {

    int *comp = (int *) calloc(inst->nnodes, sizeof(int));
    int done = 0;
    int res = 1;
    double timelimit = 5.0;

    Cbc_setMaximumSeconds(*mod, timelimit);

    while (!done) {

        res = Cbc_solve(*mod);
        const double *cur_x = Cbc_getColSolution(*mod);

        //Cbc_isProvenOptimal(*mod) ? printf("\n\n* Risolto all'OTTIMO\n") : printf("\n\n* NON risolto all'OTTIMO\n");
        // For each edge (i,j)
        set_components(inst, comp, cur_x);


        int status = -1;
        // If more than a component is found then add subtour elimination
        if (has_loop(inst, comp)) {
            cbc_add_constraint_loop(inst, *mod, comp);
            status = 0;
        }
            // If there was only one component but it was not solved up to optimality then increase
            // the time timelimit to infinity
        else {
            if (Cbc_isProvenOptimal(*mod)) done = 1;
            else status = 1;
        }

        if (!done) {
            // Clean the solution to the model, which apparently is saved in the bounds
            for (int i = 0; i < Cbc_getNumCols(*mod); i++) {
                Cbc_setColLower(*mod, i, 0.0);
                Cbc_setColUpper(*mod, i, 1.0);
            }

            char* name = inst->input_file;
            char* extname = malloc(strlen(name)+5+3);
            strcpy(extname, name);
            strcat(extname, ".loop");
            Cbc_writeLp(*mod, extname);
            Cbc_deleteModel(*mod);
            Cbc_Model *new_mod = Cbc_newModel();
            strcat(extname, ".lp");
            Cbc_readLp(new_mod, extname);
            *mod = new_mod;
            Cbc_setLogLevel(*mod, 0);
            remove(extname);
            free(extname);

        }



        if (status == 1) Cbc_setMaximumSeconds(*mod, timelimit *= 2);
        if (status == 0) Cbc_setMaximumSeconds(*mod, timelimit);

        //printf("* Constraint #: %d\n", Cbc_getNumRows(*mod));
    }

    free(comp);
    return res;
}

h_GPC_Plot *plot_cbc_sol(instance *inst, Cbc_Model *mod) {

    switch (inst->model) {
        case 1:
            return cbc_plot_symm_sol(inst, mod);
        case 2:
            return cbc_plot_asymm_sol(inst, mod);
        case 3:
            return cbc_plot_symm_sol(inst, mod);
        case 4:
            return cbc_plot_asymm_sol(inst, mod);
    }
    return NULL;

}

h_GPC_Plot *cbc_plot_symm_sol(instance *inst, Cbc_Model *mod) {

    h_GPC_Plot *plotter;
    plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

    ComplexRect_s *data = (ComplexRect_s *) calloc(inst->nnodes, sizeof(ComplexRect_s));
    ComplexRect_s edge[2];

    const double *x = Cbc_getColSolution(mod);

    for (int i = 0; i < inst->nnodes; i++) {
        data[i].real = inst->xcoord[i];
        data[i].imag = inst->ycoord[i];
    }

    // Plot the vertices
    gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
                       inst->max_y_val, inst->min_x_val, inst->min_y_val);

    // Plot the edges
    gpc_setup_plot_line(plotter);
    for (int i = 0; i < inst->nnodes; i++) {
        int count = 0;
        edge[0].real = inst->xcoord[i];
        edge[0].imag = inst->ycoord[i];

        for (int j = i + 1; j < inst->nnodes; j++) {

            int cxpos = cplex_pos_base(inst, i, j);

            if (x[cxpos] > 0.5) {
                //printf("cur_x(%d,%d)=%2.1f\n", i + 1, j + 1, cur_x[cxpos]);
                edge[1].real = inst->xcoord[j];
                edge[1].imag = inst->ycoord[j];
                gpc_plot_line(plotter, edge);
                if (++count == 2) break;
            }
        }
    }

    gpc_close_plot_line(plotter);
    free(data);

    return plotter;
}

h_GPC_Plot *cbc_plot_asymm_sol(instance *inst, Cbc_Model *mod) {

    h_GPC_Plot *plotter;
    plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

    ComplexRect_s *data = (ComplexRect_s *) calloc(inst->nnodes, sizeof(ComplexRect_s));
    ComplexRect_s edge[2];

    const double *x = Cbc_getColSolution(mod);

    for (int i = 0; i < inst->nnodes; i++) {
        data[i].real = inst->xcoord[i];
        data[i].imag = inst->ycoord[i];
    }

    // Plot the vertices
    gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
                       inst->max_y_val, inst->min_x_val, inst->min_y_val);

    // Plot the edges
    gpc_setup_plot_line(plotter);

    for (int i = 0; i < inst->nnodes; i++) {
        int count = 0;
        edge[0].real = inst->xcoord[i];
        edge[0].imag = inst->ycoord[i];

        for (int j = 0; j < inst->nnodes; j++) {
            if (i == j) continue;
            int cxpos = cplex_pos_compact_MTZ(inst, i, j);

            if (x[cxpos] > 0.5) {
                //printf("cur_x(%d,%d)\n", i + 1, j + 1);
                edge[1].real = inst->xcoord[j];
                edge[1].imag = inst->ycoord[j];
                gpc_plot_line(plotter, edge);
                if (++count == 2) break;
            }
        }
    }

    gpc_close_plot_line(plotter);
    //free(cur_x);
    free(data);

    return plotter;
}