#ifndef TSP_H
#define TSP_H

#define MODEL_BASE 1
#define MODEL_COMPACT_MTZ 2
#define MODEL_COMPACT_CUSTOM 3
#define MODEL_COMPACT_F1 4
#define MODEL_BASE_LOOP 5
#define MODEL_BASE_CALLBACK_LOOP 6
#define MODEL_BASE_HARD_FIXING 7
#define MODEL_BASE_LOCAL_BRANCHING 8


#define SOLVE_FALSE 0
#define SOLVE_TRUE 1

#define SOLVER_CPLEX 1
#define SOLVER_CBC 2
#define SOLVER_COMBO 3
#define SOLVER_NEIGHBORHOOD_SEARCH 4
#define SOLVER_SIMULATED_ANNEALING 5
#define SOLVER_GRASP 6

#include <math.h>
#include <cplex.h>
#include <string.h>
#include "util.h"

typedef struct {

	// Input Data
	int nnodes;
	double *xcoord;
	double *ycoord;
	char edge_type[50];

	// Plotting Parameters
	double max_x_val;
	double max_y_val;
	double min_x_val;
	double min_y_val;

	// Parameters
	char input_file[1000];	// Input file
	int model;				// TSP Model
	int solve;				// Solve the model?

	// Solver
	int solver; // 1->cplex 2->cbc 3->combo

} instance;

/** Initializes the model in CPLEX and stores it locally then solves the optimization problem.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
* @return zero if successful and nonzero if an error occurs.
*/
int exec_solver(instance* inst, CPXENVptr env, CPXLPptr lp);
int solve_model(instance* inst, CPXENVptr env, CPXLPptr lp);
int loop(instance* inst, CPXENVptr env, CPXLPptr lp);
int has_loop(instance* inst, const int* comp);

/** Builds the model in CPLEX by inserting all the variables and constraints.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
*/
void build_model_base(instance* inst, CPXENVptr env, CPXLPptr lp);
void build_model_compact_MTZ(instance* inst, CPXENVptr env, CPXLPptr lp);
void build_model_compact_custom(instance* inst, CPXENVptr env, CPXLPptr lp);
void build_model_compact_F1(instance* inst, CPXENVptr env, CPXLPptr lp);


/** Computes the CPLEX column index of the specified variable
*
* @param inst Pointer to an instance of the "instance" struct.
* @param v1 first vertex.
* @param v2 second vertex.
* @param v vertex.
* @param h position.
* @return CPLEX column index.
*/
int cplex_pos_base(instance* inst, int v1, int v2);
int cplex_pos_compact_MTZ(instance* inst, int v1, int v2);
int cplex_pos_compact_custom(instance* inst, int v, int h);
int cplex_pos_compact_F1(instance* inst, int v1, int v2);

/** Computes the distance between the two points
*
* @param inst Pointer to an instance of the "instance" struct.
* @param p1 first point.
* @param p2 second point.
* @return computed distance.
*/
double euc_2D(instance* inst, int p1, int p2);
double geo(instance* inst, int p1, int p2);
double att(instance* inst, int p1, int p2);
double dist(instance* inst, int p1, int p2);

/** Frees the allocated space for the input data.
*
* @param inst Pointer to an instance of the "instance" struct.
*/
void free_instance(instance *inst);

/** Checks if the problem was solved up to optimality or the process was halted before.
*
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
* @return 1 if up to optimality 0 otherwise.
*/
int mip_solved_to_optimality(CPXENVptr env, CPXLPptr lp);

/** Sets the timelimit for the computation.
*
* @param env CPLEX Environment struct.
* @param timelimit Timelimit in seconds.
*/
void mip_timelimit(CPXENVptr env, double timelimit);

/**
 * Applies Kruskal's Algorithm to separate the connected components.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param comp Pointer to an uninitialized array of ints of length n, with n the number of nodes, it will contain the
 * component of the i-th node.
 * @param x Pointer to an array containing the current solution.
  */
void set_components(instance* inst, int* comp, const double* x);

/**
 * Generate subtour elimination cuts (with loop) to be used in a CPLEX callback.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param xstar Pointer to an array containing the current solution.
 * @param env CPLEX Environment struct.
 * @param cbdata Data required by a CPLEX callback.
 * @param wherefrom Condition of invocation, required by a CPLEX callback.
 * @return Number of generated cuts.
 */
int cut_loops_lazy(instance* inst, double *xstar, CPXCENVptr env, void *cbdata, int wherefrom);

/**
 * CPLEX callback function.
 * @param env CPLEX Environment struct.
 * @param cbdata Data required by a CPLEX callback.
 * @param wherefrom Condition of invocation, required by a CPLEX callback.
 * @param cbhandle Pointer to the data needed by the callback.
 * @param useraction_p Status code given to CPLEX after the termination of the callback.
 * @return 0 if everything is fine
 */
static int CPXPUBLIC check_callback_loop(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p);


int loop(instance* inst, CPXENVptr env, CPXLPptr lp);
int cloop(instance* inst, CPXENVptr env, CPXLPptr lp);
int hcloop(instance* inst, CPXENVptr env, CPXLPptr lp);
int localbranching(instance* inst, CPXENVptr env, CPXLPptr lp);

double get_cost_from_x_symm(instance* inst, double* x);

/**
 * Applies the nearest neighbourhood algorithm, for each node chooses a node from the four closest nodes, the choice is
 * made so the smaller the distance the higher the probability of being chosen, returns the solution with the choices.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param start The starting node.
 * @return Pointer to the solution, must be freed after use.
 */
double* getNearestNeihgborhoodPath(instance* inst, int start, int NEIGHBOURHOOD);

/**
 * Computes the nodes sequence from the provided solution.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x Pointer to a solution.
 * @param path Pointer to the computed path.
 */
void calc_path(instance* inst, const double* x, int* path);

/**
 * Computes the two best edges to be swapped for a a cost improvement.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param path Pointer to a path.
 * @param from1 Pointer to the starting node of the first edge.
 * @param to1 Pointer to the arrival node of the first edge.
 * @param from2 Pointer to the starting node of the second edge.
 * @param to2 Pointer to the arrival node of the second edge.
 * @param delta Pointer to the cost improvement value.
 */
void best_edges_two_opt(instance* inst, int* path, int* from1, int* to1, int* from2, int* to2, double* delta);

/**
 * Applies the specified swap to the specified solution and updates the path.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x Pointer to the solution to be updated.
 * @param path Pointer to the path to be updated.
 * @param from1 Pointer to the starting node of the first edge.
 * @param to1 Pointer to the arrival node of the first edge.
 * @param from2 Pointer to the starting node of the second edge.
 * @param to2 Pointer to the arrival node of the second edge.
 */
void cross_edges(instance* inst, double* x, int* path, int from1, int to1, int from2, int to2);

/**
 * Applies the two opt heuristic refinement to the specified solution. The swaps always consider the best possible pairs.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x Pointer to the solution to be refined.
 * @return
 */
int two_opt(instance* inst, double* x);

/**
 * Applies a random three opt swap to the specified solution. Is implemented through two two opt swaps.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x Pointer to the solution to be kicked.
 */
void kick_three_opt(instance* inst, double* x);

/**
 * Applies the simulated annealing metaheuristic implemented through a discount on the cost of the objective function.
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x Pointer to the solution to be refined.
 */
void simulated_annealing(instance* inst, double* x);
void simulated_annealing2(instance* inst, double* x);

/**
 * Implementation of the Greedy Randomized Adaptive Search Procedure (GRASP)
 * @param inst Pointer to an instance of the "instance" struct.
 * @param prob Probability of Greedy approach.
 * @param neighbours Number of closest neighbours to choose from.
 * @param x_star Pointer to a solution array.
 * @param cost Pointer to solution cost.
 * @return
 */
int grasp(instance* inst, double prob, int neighbours, double* x_star, double* cost);


#endif // !TSP_H
