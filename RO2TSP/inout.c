#include "inout.h"

void parse_command_line(int argc, char** argv, instance *inst) {

	const int MAX_LENGTH = 20;

	// Default values
	strcpy(inst->input_file, "NULL");
	inst->model = MODEL_BASE;	// Base model
	inst->solve = SOLVE_TRUE;	// Solve the model
	inst->solver = SOLVER_CPLEX;

	int help = 0; if (argc < 1) help = 1;

	for (int i = 0; i < argc; i++) {

	    // Input File
		if (strcmp(argv[i], "-file") == 0) { strcpy(inst->input_file, argv[++i]); continue; }
		if (strcmp(argv[i], "-f") == 0) { strcpy(inst->input_file, argv[++i]); continue; }
		if (strcmp(argv[i], "-input") == 0) { strcpy(inst->input_file, argv[++i]); continue; }
		if (strcmp(argv[i], "-i") == 0) { strcpy(inst->input_file, argv[++i]); continue; }
		// Help
		if (strcmp(argv[i], "-h") == 0) { help = 1; continue; }
		if (strcmp(argv[i], "--help") == 0) { help = 1; continue; }
		// Model
		if (strcmp(argv[i], "-base") == 0) { inst->model = MODEL_BASE; continue; }
		if (strcmp(argv[i], "-mtz") == 0) { inst->model = MODEL_COMPACT_MTZ; continue; }
		if (strcmp(argv[i], "-custom") == 0) { inst->model = MODEL_COMPACT_CUSTOM; continue; }
		if (strcmp(argv[i], "-f1") == 0) { inst->model = MODEL_COMPACT_F1; continue; }
        if (strcmp(argv[i], "-loop") == 0) { inst->model = MODEL_BASE_LOOP; continue; }
        if (strcmp(argv[i], "-cloop") == 0) { inst->model = MODEL_BASE_CALLBACK_LOOP; continue; }
		if (strcmp(argv[i], "-hcloop") == 0) { inst->model = MODEL_BASE_HARD_FIXING; continue; }
		if (strcmp(argv[i], "-hlbcloop") == 0) { inst->model = MODEL_BASE_LOCAL_BRANCHING; continue; }
		// Solver
		if (strcmp(argv[i], "-s") == 0) { inst->solve = SOLVE_FALSE; continue; }
		if (strcmp(argv[i], "-cplex") == 0) { inst->solver = SOLVER_CPLEX; continue; }
		if (strcmp(argv[i], "-cbc") == 0) { inst->solver = SOLVER_CBC; continue; }
		if (strcmp(argv[i], "-combo") == 0) { inst->solver = SOLVER_COMBO; continue; }
		if (strcmp(argv[i], "-ns") == 0) { inst->solver = SOLVER_NEIGHBORHOOD_SEARCH; continue; }
		if (strcmp(argv[i], "-simann") == 0) { inst->solver = SOLVER_SIMULATED_ANNEALING; continue; }
        if (strcmp(argv[i], "-grasp") == 0) { inst->solver = SOLVER_GRASP; continue; }
		help = 0;
	}

	// Print current parameters
	if (help)
	{
		printf("\n\n AVAILABLE ARGUMENTS:\n\n");
		printf(" %-*s   Path to input file.\n", MAX_LENGTH,"-file/-f/-input/-i");
		printf(" %-*s   Selects the base TSP model without subtour elimination.\n", MAX_LENGTH, "-base");
		printf(" %-*s   Selects the compact Miller-Tucker-Zemlin TSP model.\n", MAX_LENGTH, "-mtz");
		printf(" %-*s   Selects the compact custom TSP model.\n", MAX_LENGTH, "-custom");
		printf(" %-*s   Selects the compact Single Commodity Flow (F1) TSP model.\n", MAX_LENGTH, "-f1");
        printf(" %-*s   Selects the base TSP model with 'loop' subtour elimination.\n", MAX_LENGTH, "-loop");
        printf(" %-*s   Selects the base TSP model with 'loop' subtour elimination through callbacks.\n", MAX_LENGTH, "-cloop");
        printf(" %-*s   Selects the base TSP model with 'loop' subtour elimination through callbacks and hard-fixing.\n", MAX_LENGTH, "-hcloop");
        printf(" %-*s   Selects the base TSP model with 'loop' subtour elimination through callbacks and local branching.\n", MAX_LENGTH, "-hlbcloop");
		printf(" %-*s   Deactivates model solution.\n", MAX_LENGTH, "-s");
        printf(" %-*s   Solves the model with IBM ILOG CPLEX.\n", MAX_LENGTH, "-cplex");
        printf(" %-*s   Solves the model with COIN-OR CBC.\n", MAX_LENGTH, "-cbc");
        printf(" %-*s   Solves the model with both CPLEX and CBC.\n", MAX_LENGTH, "-combo");
        printf(" %-*s   Solves the model with the Neighbourhood Search heuristic.\n", MAX_LENGTH, "-ns");
        printf(" %-*s   Solves the model with the Simulated Annealing heuristic.\n", MAX_LENGTH, "-simann");
        printf(" %-*s   Solves the model with the GRASP heuristic.\n", MAX_LENGTH, "-grasp");

		printf("\n\n CURRENT SETTINGS:\n\n");
		switch (inst->model) {
			case MODEL_BASE:
				printf(" %-*s   base TSP\n", MAX_LENGTH, "Model:");
				break;
			case MODEL_COMPACT_MTZ:
				printf(" %-*s   compact MTZ\n", MAX_LENGTH, "Model:");
				break;
			case MODEL_COMPACT_CUSTOM:
				printf(" %-*s   compact custom\n", MAX_LENGTH, "Model:");
				break;
			case MODEL_COMPACT_F1:
				printf(" %-*s   compact F1\n", MAX_LENGTH, "Model:");
				break;
            case MODEL_BASE_LOOP:
                printf(" %-*s   base with LOOP\n", MAX_LENGTH, "Model:");
                break;
            case MODEL_BASE_CALLBACK_LOOP:
                printf(" %-*s   base with LOOP in callback\n", MAX_LENGTH, "Model:");
                break;
            case MODEL_BASE_HARD_FIXING:
                printf(" %-*s   base with LOOP in callback and hard-fixing\n", MAX_LENGTH, "Model:");
                break;
            case MODEL_BASE_LOCAL_BRANCHING:
                printf(" %-*s   base with LOOP in callback and local branching\n", MAX_LENGTH, "Model:");
                break;
		}
		printf(" %-*s   %s\n", MAX_LENGTH, "Path:", inst->input_file);		
		printf(" %-*s   %s\n", MAX_LENGTH, "Solve:", (inst->solve>0)? "Yes":"No");
		switch (inst->solver) {
            case SOLVER_CPLEX:
                printf(" %-*s   CPLEX\n", MAX_LENGTH, "Solver:");
                break;
            case SOLVER_CBC:
                printf(" %-*s   CBC\n", MAX_LENGTH, "Solver:");
                break;
            case SOLVER_COMBO:
                printf(" %-*s   CPLEX and CBC\n", MAX_LENGTH, "Solver:");
                break;
            case SOLVER_NEIGHBORHOOD_SEARCH:
                printf(" %-*s   Neighbourhood Search\n", MAX_LENGTH, "Solver:");
                break;
            case SOLVER_SIMULATED_ANNEALING:
                printf(" %-*s   Simulated Annealing\n", MAX_LENGTH, "Solver:");
                break;
            case SOLVER_GRASP:
                printf(" %-*s   GRASP\n", MAX_LENGTH, "Solver:");
                break;
		}
		printf("\nEnter -h or --help for help\n\n");
	}

}

void read_input(instance *inst) {

	FILE *fin = fopen(inst->input_file, "r");
	if (fin == NULL) print_error(" input file not found!");

	// Placeholder values
	inst->nnodes = -1;
	inst->max_x_val = -1;
	inst->max_y_val = -1;
	inst->min_x_val = 0;
	inst->min_y_val = 0;

	char line[180];
	char *par_name;
	char *token1;
	char *token2;

	int active_section = 0; // =1 NODE_COORD_SECTION

	while (fgets(line, sizeof(line), fin) != NULL)
	{
		// Skip empty lines
		if (strlen(line) <= 1) continue;

		par_name = strtok(line, " :");

		if (strncmp(par_name, "NAME", 4) == 0)
		{
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "COMMENT", 7) == 0)
		{
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "TYPE", 4) == 0)
		{
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "DIMENSION", 9) == 0)
		{
			if (inst->nnodes >= 0) print_error(" repeated DIMENSION section in input file");
			token1 = strtok(NULL, " :");
            inst->nnodes = strtol(token1, NULL, 10);
			inst->xcoord = (double *)calloc(inst->nnodes, sizeof(double));
			inst->ycoord = (double *)calloc(inst->nnodes, sizeof(double));
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "EDGE_WEIGHT_TYPE", 16) == 0)
		{
			token1 = strtok(NULL, " :");
			strcpy(inst->edge_type, token1);
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "DISPLAY_DATA_TYPE", 17) == 0)
		{
			active_section = 0;
			continue;
		}

		if (strncmp(par_name, "NODE_COORD_SECTION", 18) == 0)
		{
			if (inst->nnodes <= 0) print_error(" ... DIMENSION section should appear before NODE_COORD_SECTION section");
			active_section = 1;
			continue;
		}

		if (strncmp(par_name, "EOF", 3) == 0)
		{
			break;
		}

		
		if (active_section == 1) // within NODE_COORD_SECTION
		{
			int i = strtol(par_name, NULL, 10) - 1;
			if (i < 0 || i >= inst->nnodes) print_error(" ... unknown node in NODE_COORD_SECTION section");
			token1 = strtok(NULL, " :,");
			token2 = strtok(NULL, " :,");
			inst->xcoord[i] = strtod(token1, NULL);
			inst->ycoord[i] = strtod(token2, NULL);
			if (inst->xcoord[i] > inst->max_x_val) inst->max_x_val = inst->xcoord[i];
			if (inst->ycoord[i] > inst->max_y_val) inst->max_y_val = inst->ycoord[i];
			if (inst->xcoord[i] < inst->min_x_val) inst->min_x_val = inst->xcoord[i];
			if (inst->ycoord[i] < inst->min_y_val) inst->min_y_val = inst->ycoord[i];
			if(VERBOSE>=200) printf(" ... node %4d at coordinates ( %15.7lf , %15.7lf )\n", i + 1, inst->xcoord[i], inst->ycoord[i]);
			continue;
		}

		print_error(" ... wrong format for the current parser!");

	}

	fclose(fin);
}