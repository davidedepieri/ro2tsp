#include "tsp.h"

// TSP Utilities

double euc_2D(instance* inst, int p1, int p2) {
	double x1 = inst->xcoord[p1];
	double x2 = inst->xcoord[p2];
	double y1 = inst->ycoord[p1];
	double y2 = inst->ycoord[p2];
	return (int)(0.5 + sqrt(pow(x1 - x2, 2) + pow(y1 - y2, 2)));
}

double geo(instance* inst, int p1, int p2) {
	const double PI = 3.1415926535897;
	const double RRR = 6378.388;

	double deg = (int)(inst->xcoord[p1]);
	double min = inst->xcoord[p1] - deg;
	double lat_p1 = PI * (deg + 5.0*min / 3.0) / 180.0;

	deg = (int)(inst->ycoord[p1]);
	min = inst->ycoord[p1] - deg;
	double lon_p1 = PI * (deg + 5.0*min / 3.0) / 180.0;

	deg = (int)(inst->xcoord[p2]);
	min = inst->xcoord[p2] - deg;
	double lat_p2 = PI * (deg + 5.0*min / 3.0) / 180.0;

	deg = (int)(inst->ycoord[p2]);
	min = inst->ycoord[p2] - deg;
	double lon_p2 = PI * (deg + 5.0*min / 3.0) / 180.0;
	
	double q1 = cos(lon_p1 - lon_p2);
	double q2 = cos(lat_p1 - lat_p2);
	double q3 = cos(lat_p1 + lat_p2);
	double d_p1_p2 = (int)(RRR*acos(0.5*((1.0 + q1)*q2 - (1.0 - q1)*q3)) + 1.0);

	return d_p1_p2;
}

double att(instance* inst, int p1, int p2) {
	double xd = inst->xcoord[p1] - inst->xcoord[p2];
	double yd = inst->ycoord[p1] - inst->ycoord[p2];
	double rij = sqrt((pow(xd, 2) + pow(yd, 2)) / 10.0);
	int tij = (int)(0.5 + rij);
	if (tij < rij) return tij + 1;
	else return tij;
}

double dist(instance* inst, int p1, int p2) {
	if (strncmp(inst->edge_type, "EUC_2D", 6) == 0) return euc_2D(inst, p1, p2);
	if (strncmp(inst->edge_type, "GEO", 3) == 0) return geo(inst, p1, p2);
	if (strncmp(inst->edge_type, "ATT", 3) == 0) return att(inst, p1, p2);

	print_error(" distance type not supported");
	return 1;
}

double get_cost_from_x_symm(instance* inst, double* x) {
	double res = 0.0;
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = i + 1; j < inst->nnodes; j++) {
			if (x[cplex_pos_base(inst, i, j)] > 0.5)
				res += dist(inst, i, j);
		}
	}
	return res;
}

int mip_solved_to_optimality(CPXENVptr env, CPXLPptr lp) {

	int lpstat = CPXgetstat(env, lp);
	int solved = (lpstat == CPXMIP_OPTIMAL) ||
				 (lpstat == CPXMIP_OPTIMAL_INFEAS) ||
			   //(lpstat == CPXMIP_OPTIMAL_RELAXED) ||
				 (lpstat == CPXMIP_OPTIMAL_TOL);
	return solved;
}

void mip_timelimit(CPXENVptr env, double timelimit) {

	CPXsetintparam(env, CPX_PARAM_CLOCKTYPE, 2);				// Wall clock time (total physical time elapsed)
	CPXsetdblparam(env, CPX_PARAM_TILIM, timelimit);			// Real time
	//CPXsetdblparam(env, CPX_PARAM_DETTILIM, 1000*timelimit);	// Ticks
}

int cplex_pos_base(instance* inst, int v1, int v2) {

	if (v1 == v2) print_error(" variable indexes cannot be the same");
	if (v1 > v2) return cplex_pos_base(inst, v2, v1);

	return v1 * inst->nnodes + v2 - (v1 + 1)*(v1 + 2) / 2;
}

int cplex_pos_compact_MTZ(instance* inst, int v1, int v2) {
	return v1 * inst->nnodes + v2;
}

int cplex_pos_compact_custom(instance* inst, int v, int h) {

	int offset = inst->nnodes * (inst->nnodes - 1) / 2;
	
	if (v == 0 && h == 0) return offset;
	if (v == 0 || h == 0) print_error(" the first node and first position are always fixed");

	return offset + (inst->nnodes - 1) * (v - 1) + h;
}

int cplex_pos_compact_F1(instance* inst, int v1, int v2) {

	int offset = inst->nnodes * inst->nnodes;
	return offset + v1 * inst->nnodes + v2;
}


// Model Builders

void build_model_base(instance* inst, CPXENVptr env, CPXLPptr lp) {

	// VARIABLES DEFINITION
	const double lower_bound_x = 0.0;					// Lower bound of the variables
	const double upper_bound_x = 1.0;					// Upper bound of the variables
	const char binary = 'B';
	char **cname = (char **)calloc(1, sizeof(char *));	// Allocation of the string array for the variable label
	cname[0] = (char *)calloc(100, sizeof(char));		// Allocation of the single array element

	// Create all the variables x(j,i) corresponding to the edges from the node j to i
	// ingnoring self loops and considering the edges as not oriented
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = i + 1; j < inst->nnodes; j++) {

			sprintf(cname[0], "x(%d,%d)", i + 1, j + 1);	// +1 index shift to start counting nodes from one
			double cost = dist(inst, i, j);				// The cost is the distance between the nodes

			if ( CPXnewcols(env, lp, 1, &cost, &lower_bound_x, &upper_bound_x, &binary, cname) ) print_error(" column creation failed");
			if ( CPXgetnumcols(env, lp) - 1 != cplex_pos_base(inst, i, j) ) print_error(" wrong position for x var.s");
		}
	}
	
	// CONSTRAINTS DEFINITION
	const double b = 2.0;
	const char sense = 'E';

	// For each vertex v
	for (int v = 0; v < inst->nnodes; v++) {

		int last_row = CPXgetnumrows(env, lp);		
		sprintf(cname[0], "vertice(%d)", v + 1);

		if (CPXnewrows(env, lp, 1, &b, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each vertex u different from v
		for (int u = 0; u < inst->nnodes; u++) {
			if ( (u != v) && CPXchgcoef(env, lp, last_row, cplex_pos_base(inst, u, v), 1.0) ) print_error(" wrong CPXchgcoef");			
		}
	}

	free(cname[0]);
	free(cname);
}

void build_model_compact_custom(instance* inst, CPXENVptr env, CPXLPptr lp) {

	// VARIABLE DEFINITION
	const double lower_bound_x = 0.0;					// Lower bound of the variables
	const double upper_bound_x = 1.0;					// Upper bound of the variables
	const char binary = 'B';
	char **cname = (char **)calloc(1, sizeof(char *));	// Allocation of the string array for the variable label
	cname[0] = (char *)calloc(100, sizeof(char));		// Allocation of the single array element

	// Create all the variables x(j,i) corresponding to the edges from the node j to i
	// ingnoring self loops and considering the edges as not oriented
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = i + 1; j < inst->nnodes; j++) {

			sprintf(cname[0], "x(%d,%d)", i + 1, j + 1);	// +1 index shift to start counting nodes from one
			double cost = dist(inst, i, j);					// The cost is the distance between the nodes

			if (CPXnewcols(env, lp, 1, &cost, &lower_bound_x, &upper_bound_x, &binary, cname)) print_error(" column creation failed");
			if (CPXgetnumcols(env, lp) - 1 != cplex_pos_base(inst, i, j)) print_error(" wrong position for x var.s");
		}
	}
	
	// Create all the variables z(v,h) wich signal if node v is in position h
	for (int v = 0; v < inst->nnodes; v++) {
		for (int h = 0; h < inst->nnodes; h++) {

			// The first variable is fixed, skip the same index
			if ((v == 0 && h != 0) || (v != 0 && h == 0)) continue;

			sprintf(cname[0], "z(%d,%d)", v + 1, h + 1);	// +1 index shift to start counting nodes from one
			double cost = 0;

			if (CPXnewcols(env, lp, 1, &cost, (v == 0 && h == 0) ? &upper_bound_x : &lower_bound_x, &upper_bound_x, &binary, cname)) print_error(" column creation failed");
			if (CPXgetnumcols(env, lp) - 1 != cplex_pos_compact_custom(inst, v, h)) print_error(" wrong position for z var.s");
		}
	}

	// CONSTRAINTS DEFINITION

	// Static Constraints
	const double b = 2.0;
	const double d = 1.0;
	const char sense_e = 'E';

	// For each vertex v
	for (int v = 0; v < inst->nnodes; v++) {

		int last_row = CPXgetnumrows(env, lp);
		sprintf(cname[0], "vertice(%d)", v + 1);

		if (CPXnewrows(env, lp, 1, &b, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each vertex u different from v
		for (int u = 0; u < inst->nnodes; u++) {
			if ((u != v) && CPXchgcoef(env, lp, last_row, cplex_pos_base(inst, u, v), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}

	// Each vertex must have only one associated position
	for (int v = 1; v < inst->nnodes; v++) {

		int last_row = CPXgetnumrows(env, lp);
		sprintf(cname[0], "vertice_pos(%d)", v + 1);

		if (CPXnewrows(env, lp, 1, &d, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each position h
		for (int h = 1; h < inst->nnodes; h++) {
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_custom(inst, v, h), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}

	// Each position must have only one associated vertex
	for (int h = 1; h < inst->nnodes; h++) {

		int last_row = CPXgetnumrows(env, lp);
		sprintf(cname[0], "pos_vertice(%d)", h + 1);

		if (CPXnewrows(env, lp, 1, &d, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each vertex v
		for (int v = 1; v < inst->nnodes; v++) {
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_custom(inst, v, h), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}

	// Node one is close to the nodes in position two and n
	const char sense_l = 'L';
	
	for (int i = 1; i < inst->nnodes; i++) {

		int last_row = CPXgetnumrows(env, lp);
		sprintf(cname[0], "first_node(%d)", i + 1);

		if (CPXnewrows(env, lp, 1, &d, &sense_l, NULL, cname)) print_error(" wrong CPXnewrows");

		if (CPXchgcoef(env, lp, last_row, cplex_pos_base(inst, i, 0), 1.0)) print_error(" wrong CPXchgcoef");

		// For each position t
		for (int t = 2; t < inst->nnodes - 1; t++) {
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_custom(inst, i, t), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}
	
	// LAZY CONSTRAINTS
	const int rcnt = 1;
	const int nzcnt = inst->nnodes - 1;
	const double rhs = 2.0;
	const int rmatbeg = 0;

	int* rmatind = (int*)calloc(nzcnt, sizeof(int));
	double* rmatval = (double*)calloc(nzcnt, sizeof(double));
	for (int k = 0; k < nzcnt; k++) rmatval[k] = 1.0;
	
	int count = 0;
	for (int h = 1; h < inst->nnodes-2; h++) {
		for (int i = 1; i < inst->nnodes; i++) {
			for (int j = 1; j < inst->nnodes; j++) {

				if (i == j) continue;
				
				for (int t = 1; t <= h; t++) rmatind[t - 1] = cplex_pos_compact_custom(inst, i, t);
				rmatind[h] = cplex_pos_base(inst, i, j);
				for (int t = h + 2; t < inst->nnodes; t++) rmatind[t - 1] = cplex_pos_compact_custom(inst, j, t);
				
				sprintf(cname[0], "lazy(h=%d,i=%d,j=%d)", h+1, i+1, j+1);
				if (++count % 10000 == 0) if (VERBOSE >= 200) printf("it: %d\n", count);
				if (CPXaddlazyconstraints(env, lp, rcnt, nzcnt, &rhs, &sense_l, &rmatbeg, rmatind, rmatval, cname)) print_error(" wrong CPXaddlazy");
			}
		}
	}
	
	free(rmatind);
	free(rmatval);
	free(cname[0]);
	free(cname);
}

void build_model_compact_MTZ(instance* inst, CPXENVptr env, CPXLPptr lp) {

	// VARIABLES DEFINITION
	const double lower_bound_y = 0.0;					// Lower bound of the variables
	const double upper_bound_y = 1.0;					// Upper bound of the variables
	const char binary = 'B';
	char **cname = (char **)calloc(1, sizeof(char *));	// Allocation of the string array for the variable label
	cname[0] = (char *)calloc(100, sizeof(char));		// Allocation of the single array element

	// Create all the variables y(j,i) corresponding to the edges from the node j to i
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = 0; j < inst->nnodes; j++) {

			sprintf(cname[0], "y(%d,%d)", i + 1, j + 1);	// +1 index shift to start counting nodes from one
			double cost = dist(inst, i, j);					// The cost is the distance between the nodes

			if (CPXnewcols(env, lp, 1, &cost, &lower_bound_y, ((i==j)? &lower_bound_y : &upper_bound_y), &binary, cname)) print_error(" column creation failed");
			if (CPXgetnumcols(env, lp) - 1 != cplex_pos_compact_MTZ(inst, i, j)) print_error(" wrong position for y var.s");
		}
	}
	
	const double lower_bound_u = 0.0;
	const double upper_bound_u = inst->nnodes - 1;
	const double cost = 0;
	const char integer = 'I';

	// Create all variables u(j) corriesponding to the position of the j-th node in the circuit
	for (int i = 0; i < inst->nnodes; i++) {
		sprintf(cname[0], "u(%d)", i + 1);
		if (CPXnewcols(env, lp, 1, &cost, &lower_bound_u, (i == 0) ? &lower_bound_u : &upper_bound_u, &integer, cname)) print_error(" column creation failed");
	}
	
	// CONSTRAINTS DEFINITION

	// Static Constraints
	const double b = 1.0;
	const char sense = 'E';

	// For each vertex v
	for (int v = 0; v < inst->nnodes; v++) {

		int last_row = CPXgetnumrows(env, lp);

		sprintf(cname[0], "vertice(%d) archi entranti", v + 1);
		if (CPXnewrows(env, lp, 1, &b, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

		sprintf(cname[0], "vertice(%d) archi uscenti", v + 1);
		if (CPXnewrows(env, lp, 1, &b, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each vertex u different from v
		for (int u = 0; u < inst->nnodes; u++) {
			if (u == v) continue;
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_MTZ(inst, u, v), 1.0)) print_error(" wrong CPXchgcoef");
			if (CPXchgcoef(env, lp, last_row+1, cplex_pos_compact_MTZ(inst, v, u), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}
	
	
	
	// Lazy Constraints

	// y(j,i) + y(i,j) <= 1
	const int rcnt = 1;	// Number of lazy constraints to add at each iteration
	int nzcnt = 2;		// Number of nonzero coefficient in the lazy constraint
	double rhs = 1.0;
	const char sense_l = 'L';
	const int rmatbeg = 0; 
	int rmatind[2];
	double rmatval[2] = { 1.0, 1.0 };
	
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = i + 1; j < inst->nnodes; j++) {
			rmatind[0] = cplex_pos_compact_MTZ(inst, i, j);
			rmatind[1] = cplex_pos_compact_MTZ(inst, j, i);
			sprintf(cname[0], "lazy y(%d,%d)+y(%d,%d)<=1", i, j, j, i);
			if (CPXaddlazyconstraints(env, lp, rcnt, nzcnt, &rhs, &sense_l, &rmatbeg, rmatind, rmatval, cname)) print_error(" wrong CPXaddlazy");
		}
	}
	
	// u(j) - u(i) - bigM*y(i,j) >= 1 - bigM
	nzcnt = 3;
	const int bigM = inst->nnodes-1;
	rhs = 1 - bigM;
	const char sense_g = 'G';
	int rmatindU[3];
	double rmatvalU[3] = { 1.0, -1.0, -bigM };
	int offset = (int) pow(inst->nnodes, 2);	// to skip the y(j,i) variables

	for (int j = offset + 1; j < offset + inst->nnodes; j++) {
		for (int i = offset; i < offset + inst->nnodes; i++) {
			if (j == i) continue;
			rmatindU[0] = j; rmatindU[1] = i; rmatindU[2] = cplex_pos_compact_MTZ(inst, i - offset, j - offset);
			sprintf(cname[0], "lazy u(%d,%d)", j - offset, i - offset);
			if (CPXaddlazyconstraints(env, lp, rcnt, nzcnt, &rhs, &sense_g, &rmatbeg, rmatindU, rmatvalU, cname)) print_error(" wrong CPXaddlazy");
			
		}
	}

	free(cname[0]);
	free(cname);
}

void build_model_compact_F1(instance* inst, CPXENVptr env, CPXLPptr lp) {

	// VARIABLES DEFINITION
	const double lower_bound_x = 0.0;					// Lower bound of the variables
	const double upper_bound_x = 1.0;					// Upper bound of the variables
	const char binary = 'B';
	char **cname = (char **)calloc(1, sizeof(char *));	// Allocation of the string array for the variable label
	cname[0] = (char *)calloc(100, sizeof(char));		// Allocation of the single array element

	// Create all the variables x(j,i) corresponding to the edges from the node j to i
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = 0; j < inst->nnodes; j++) {

			sprintf(cname[0], "x(%d,%d)", i + 1, j + 1);	// +1 index shift to start counting nodes from one
			double cost = dist(inst, i, j);					// The cost is the distance between the nodes

			if (CPXnewcols(env, lp, 1, &cost, &lower_bound_x, ((i == j) ? &lower_bound_x : &upper_bound_x), &binary, cname)) print_error(" column creation failed");
			if (CPXgetnumcols(env, lp) - 1 != cplex_pos_compact_MTZ(inst, i, j)) print_error(" wrong position for x var.s");
		}
	}

	const double lower_bound_y = 0.0;
	const double upper_bound_y = inst->nnodes - 1;
	const char continuous = 'C';
	const double cost = 0;	// The cost is the distance between the nodes

	// Create all the variables y(j,i) corresponding to the flow from the node j to i
	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = 0; j < inst->nnodes; j++) {

			sprintf(cname[0], "y(%d,%d)", i + 1, j + 1);	// +1 index shift to start counting nodes from one
			
			if (CPXnewcols(env, lp, 1, &cost, &lower_bound_y, ((i == j) ? &lower_bound_y : &upper_bound_y), &continuous, cname)) print_error(" column creation failed");
			if (CPXgetnumcols(env, lp) - 1 != cplex_pos_compact_F1(inst, i, j)) print_error(" wrong position for x var.s");
		}
	}

	// CONSTRAINTS DEFINITION

	// Static Constraints
	const double b = 1.0;
	const char sense_e = 'E';

	// For each vertex v
	for (int v = 0; v < inst->nnodes; v++) {

		int last_row = CPXgetnumrows(env, lp);

		sprintf(cname[0], "vertice(%d) archi entranti", v + 1);
		if (CPXnewrows(env, lp, 1, &b, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		sprintf(cname[0], "vertice(%d) archi uscenti", v + 1);
		if (CPXnewrows(env, lp, 1, &b, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		// For each vertex u different from v
		for (int u = 0; u < inst->nnodes; u++) {
			if (u == v) continue;
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_MTZ(inst, u, v), 1.0)) print_error(" wrong CPXchgcoef");
			if (CPXchgcoef(env, lp, last_row+1, cplex_pos_compact_MTZ(inst, v, u), 1.0)) print_error(" wrong CPXchgcoef");
		}
	}
	
	// Constraints on maxflow on an edge
	const char sense_l = 'L';
	const double z = 0.0;

	for (int i = 0; i < inst->nnodes; i++) {
		for (int j = 0; j < inst->nnodes; j++) {
			if (i == j) continue;
			int last_row = CPXgetnumrows(env, lp);
			sprintf(cname[0], "arco(%d,%d) massimo flusso", i+1,j+1);
			if (CPXnewrows(env, lp, 1, &z, &sense_l, NULL, cname)) print_error(" wrong CPXnewrows");
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_F1(inst, i, j), 1.0)) print_error(" wrong CPXchgcoef");
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_MTZ(inst, i, j), (i == 0 || j == 0) ? -inst->nnodes + 1 : -inst->nnodes + 2)) print_error(" wrong CPXchgcoef");
		}
	}
	
	// Constraints on maxflow out of edge 1
	const double t = inst->nnodes - 1;

	sprintf(cname[0], "somma flussi uscenti dal nodo 1");
	if (CPXnewrows(env, lp, 1, &t, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");
	for (int j = 1; j < inst->nnodes; j++) {
		if (CPXchgcoef(env, lp, CPXgetnumrows(env,lp)-1, cplex_pos_compact_F1(inst, 0, j), 1.0)) print_error(" wrong CPXchgcoef");
	}
	
	// Constraints on income and outcome flow
	for (int j = 1; j < inst->nnodes; j++) {
		int last_row = CPXgetnumrows(env, lp);
		sprintf(cname[0], "vertice(%d) flusso entrante-uscente", j+1);
		if (CPXnewrows(env, lp, 1, &b, &sense_e, NULL, cname)) print_error(" wrong CPXnewrows");

		for (int i = 0; i < inst->nnodes; i++) {	// Archi entranti
			if (i == j) continue;
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_F1(inst, i, j), 1.0)) print_error(" wrong CPXchgcoef");
		}
		for (int i = 0; i < inst->nnodes; i++) {	// Archi uscenti
			if (i == j) continue;
			if (CPXchgcoef(env, lp, last_row, cplex_pos_compact_F1(inst, j, i), -1.0)) print_error(" wrong CPXchgcoef");
		}
	}

	free(cname[0]);
	free(cname);
}

void set_components(instance* inst, int* comp, const double* x) {
	// Initializazion of the comp[] array
	for (int i = 0; i < inst->nnodes; i++) comp[i] = i;
	for (int i = 0; i < inst->nnodes; i++) {
		int edge_counter = 0;	// # of edges in which the node i participates
		for (int j = 0; j < inst->nnodes; j++) {

			if (i == j) continue;
			int cxpos = cplex_pos_base(inst, i, j);
			//CPXgetx(env, lp, x, cxpos, cxpos);

			if (x[cxpos] > 0.5) {	// The edge (i,j) is in the solution
				int c1 = comp[i]; int c2 = comp[j];
				if (c1 != c2) { // But the nodes i,j are not in the same component

					// All the nodes in the second component are moved in the first one
					for (int k = 0; k < inst->nnodes; k++)
						if (comp[k] == c2) comp[k] = c1;
				}
				// If the node i has already participated in two edges don't bother continuing
				if (++edge_counter == 2) break;
			}
		}
	}
}


// Loop and Utilities

int has_loop(instance* inst, const int* comp) {
    for (int i = 1; i < inst->nnodes; i++)
        if (comp[i] != comp[i - 1]) {
            //if(VERBOSE>=200) printf("! At least one LOOP was found.\n");
            return 1;
        }
    return 0;
}

void add_constaint_loop(instance* inst, CPXENVptr env, CPXLPptr lp, int* comp) {

    if(VERBOSE>=200) printf("+ Adding subtour elimination\n");
    const char sense_l = 'L';
    const double rhs = -1.0;
    char **cname = (char **)calloc(1, sizeof(char *));	// Allocation of the string array for the variable label
    cname[0] = (char *)calloc(100, sizeof(char));		// Allocation of the single array element
    // Tracks the subtour constraint (its row) associated to a specific subtour
    // Worst Case: n subtours
    int* index_rows = (int*)calloc(inst->nnodes, sizeof(int));
    int prog = 0;	// Number of subtour constraints added

    for (int i = 0; i < inst->nnodes; i++) {

        // If this subtour is not in a constraint create one and keep track of it
        if (index_rows[comp[i]] == 0) {
            index_rows[comp[i]] = CPXgetnumrows(env, lp);
            sprintf(cname[0], "subtour_constraint_%d", prog++);
            if (CPXnewrows(env, lp, 1, &rhs, &sense_l, NULL, cname)) print_error(" wrong CPXnewrows");
        }

        // Update the rhs for the constraint
        double new_rhs;
        CPXgetrhs(env, lp, &new_rhs, index_rows[comp[i]], index_rows[comp[i]]);
        new_rhs += 1.0;
        CPXchgrhs(env, lp, 1, index_rows + comp[i], &new_rhs);

        // If the node is in the same component then switch to one the corresponding value
        // of the variable in the constraint of the corresponding component
        for (int j = i + 1; j < inst->nnodes; j++) {
            if (comp[i] == comp[j]) {
                CPXchgcoef(env, lp, index_rows[comp[i]], cplex_pos_base(inst, i, j), 1.0);
            }
        }
    }

    free(cname[0]);
    free(cname);
    free(index_rows);
}

int loop(instance* inst, CPXENVptr env, CPXLPptr lp) {

    int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    double* x = (double*)calloc(inst->nnodes*(inst->nnodes-1)/2, sizeof(double));
    int done = 0;
    int res = 1;
    double timelimit = 30.0;

    mip_timelimit(env, timelimit);

    while (!done) {

        res = CPXmipopt(env, lp);

        if (mip_solved_to_optimality(env, lp)) { if (VERBOSE >= 200) printf("\n\n* Risolto all'OTTIMO\n"); }
        else { if (VERBOSE >= 200) printf("\n\n* NON risolto all'OTTIMO\n"); }

        CPXgetx(env, lp, x, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1);

        set_components(inst, comp, x);

        // Print the component of each node
        if (VERBOSE >= 200)
        {for (int i = 0; i < inst->nnodes; i++) printf("%d ", comp[i]); printf("\n");}
        // If more than a component is found then add subtour elimination
        if (has_loop(inst, comp)) {
            // With timelimit deactivated the problem is always solved up to optimality.
            // IT SHOULD NEVER ENTER THIS IF STATEMENT, in the ELSE statement DONE is set to 1
            add_constaint_loop(inst, env, lp, comp);
            timelimit = 30.0;
            mip_timelimit(env, timelimit);
        }
            // If there was only one component but it was not solved up to optimality then increase
            // the time timelimit to infinity
        else { (mip_solved_to_optimality(env, lp)) ? done = 1 : mip_timelimit(env, timelimit*=2); }

        if (VERBOSE >= 200) printf("* Constraint #: %d\n", CPXgetnumrows(env, lp));
        if(VERBOSE>=200) CPXwriteprob(env, lp, "TSP_model_loop.lp", NULL);
    }

    free(comp);
    free(x);
    return res;
}


// Loop with callbacks

int cut_loops_lazy(instance* inst, double *xstar, CPXCENVptr env, void *cbdata, int wherefrom) {

	int* comp = (int*)calloc(inst->nnodes, sizeof(int));
	set_components(inst, comp, xstar);

	int ncuts = 0;  // # of cuts = # of subtours (components)
	if (!has_loop(inst, comp)) return ncuts;
	

    const char sense_l = 'L';
	// For each component it tracks the number of non-zero elements in the corresponding constraint
	// (all the edges in the subtour) (Length = nnodes)
	int* nzcnt = (int*)calloc(inst->nnodes, sizeof(int));
	// For each component it tracks |S|-1, the right-hand side of the constraint
	int* rhs = (int*)calloc(inst->nnodes, sizeof(int));
	// For each component it initializes a pointer to the indices of the non-zero elements of the constraint.
	// If a component does not represent a subtour the inmost array is not allocated
	int** cutind = (int**)calloc(inst->nnodes, sizeof(int*));
	// Same as cutind but for the coefficients of the constraints
	double** cutval = (double**)calloc(inst->nnodes, sizeof(double*));

	for (int i = 0; i < inst->nnodes; i++) {
		if (nzcnt[comp[i]] == 0) {  // Then initialize constraint for this subtour
			ncuts++;
			for (int j = i + 1; j < inst->nnodes; j++) 
				if(comp[i] == comp[j]) 
					rhs[comp[i]]++; // Increment right-hand side
			cutind[comp[i]] = (int*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(int));
			cutval[comp[i]] = (double*)calloc(rhs[comp[i]] * (rhs[comp[i]] + 1) / 2, sizeof(double));
		}
		for (int j = i + 1; j < inst->nnodes; j++) {
			if (comp[i] == comp[j]) {   // Then update the coefficients in the corresponding cutind and cutval constraints
				cutind[comp[i]][nzcnt[comp[i]]] = cplex_pos_base(inst, i, j);
				cutval[comp[i]][nzcnt[comp[i]]++] = 1.0;
			}
		}
	}

	for (int i = 0; i < inst->nnodes; i++) { // Add the cuts
		if (nzcnt[i] == 0) continue; // If component was empty continue
		if (CPXcutcallbackadd(env, cbdata, wherefrom, nzcnt[i], rhs[i], sense_l, cutind[i], cutval[i], 0))
		    print_error(" cutcallbackadd error");
	}
	
	free(*cutval);	free(cutval);
	free(*cutind);	free(cutind);
	free(rhs);
	free(nzcnt);
	free(comp);

	return ncuts;
}

static int CPXPUBLIC check_callback_loop(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p) {

	*useraction_p = CPX_CALLBACK_DEFAULT;   // Default status
	instance* inst = (instance *)cbhandle;  // Casting to instance struct
	double *xstar = (double*)malloc(((inst->nnodes*(inst->nnodes - 1)) / 2) * sizeof(double));  // Get current solution
	if (CPXgetcallbacknodex(env, cbdata, wherefrom, xstar, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1)) return 1;

	int ncuts = cut_loops_lazy(inst, xstar, env, cbdata, wherefrom);

	free(xstar);
	if (ncuts) *useraction_p = CPX_CALLBACK_SET;    // Change the status so that CPLEX knows about the added cuts
	
	return 0;
}

int cloop(instance* inst, CPXENVptr env, CPXLPptr lp) {

    CPXsetintparam(env, CPX_PARAM_MIPCBREDLP, CPX_OFF); // Let MIP callbacks work on the original (non reduced) model
    CPXsetlazyconstraintcallbackfunc(env, check_callback_loop, inst);   // Install a new constraint callback
    int ncores = 1; CPXgetnumcores(env, &ncores);   // Reset the number of threads as multithreading is deactivated upon
    CPXsetintparam(env, CPX_PARAM_THREADS, ncores); // installing a new constraint

    int res = CPXmipopt(env, lp);

    if(VERBOSE>=200) CPXwriteprob(env, lp, "TSP_model_loop.lp", NULL);

    return res;
}


// Neighbourhood Search and two-opt/three-opt utilities

double* getNearestNeihgborhoodPath(instance* inst, int start, int NEIGHBOURHOOD) {

	srand(time(NULL));

	double* x = (double*)calloc(inst->nnodes*(inst->nnodes - 1) / 2, sizeof(double*));
	int* visited_nodes = (int*)calloc(inst->nnodes, sizeof(int));
	int pos = start;
	visited_nodes[pos] = 1;


	for (int i = 0; i < inst->nnodes - 1; i++) {

		double* minDist = (double*)calloc(NEIGHBOURHOOD, sizeof(double));
		int* minDist_index = (int*)calloc(NEIGHBOURHOOD, sizeof(int));

		for (int j = 0; j < inst->nnodes; j++) {
			if (visited_nodes[j]) continue; // Skip already visited nodes

			double distance = dist(inst, pos, j);
			// If the current distance is smaller than the largest in the array or the array was not filled yet
			if (distance < minDist[NEIGHBOURHOOD - 1] || minDist[NEIGHBOURHOOD - 1] == 0.0) {
                // Replace its value with the current distance.
				minDist[NEIGHBOURHOOD - 1] = distance; minDist_index[NEIGHBOURHOOD - 1] = j;
				// Swap values such that [neighbours - 1] always holds the next value to be replaced, which might be
				// zero, if the array was not filled yet, or the biggest distance currently in the array;
				// The index zero holds the smallest distance currently in the array, it is followed by increasing
				// values of distances or zeros if the array was not filled yet.
				for (int t = NEIGHBOURHOOD - 2; t >= 0; t--)
					if (minDist[t] > minDist[t + 1] || minDist[t] == 0.0) {
						double h = minDist[t + 1];
						minDist[t + 1] = minDist[t];
						minDist[t] = h;
						int k = minDist_index[t + 1];
						minDist_index[t + 1] = minDist_index[t];
						minDist_index[t] = k;
					}
					else break;
			}
		}
		double sumDist = 0.0;
		// To give more weight to smallest values of distance use the multiplicative inverses and apply normalization
		for (int u = 0; u < NEIGHBOURHOOD; u++) if (minDist[u] > 0) sumDist += 1 / minDist[u];
		for (int u = 0; u < NEIGHBOURHOOD; u++) if (minDist[u] > 0) minDist[u] = (1 / minDist[u]) / sumDist;
		for (int u = 1; u < NEIGHBOURHOOD; u++) minDist[u] += minDist[u - 1];

		double random = (double)rand(); random = random / RAND_MAX;
		int selected_index;
		for (selected_index = 0; selected_index < NEIGHBOURHOOD; selected_index++)
			if (random < minDist[selected_index]) break;

		int nextNode = minDist_index[selected_index];
		x[cplex_pos_base(inst, pos, nextNode)] = 1.0;
		visited_nodes[nextNode] = 1;

		pos = nextNode;
		free(minDist);
		free(minDist_index);
	}
	
	x[cplex_pos_base(inst, pos, start)] = 1.0;

	free(visited_nodes);
	return x;
}


void cross_edges(instance* inst, double* x, int* path, int from1, int to1, int from2, int to2) {
	x[cplex_pos_base(inst, path[from1], path[to1])] = 0.0;
	x[cplex_pos_base(inst, path[from2], path[to2])] = 0.0;
	x[cplex_pos_base(inst, path[from2], path[from1])] = 1.0;
	x[cplex_pos_base(inst, path[to2], path[to1])] = 1.0;
	// When swapping nodes in a path the nodes inbetween must be reversed
	while (from2 > to1) {
		int p = path[to1];
		path[to1] = path[from2];
		path[from2] = p;
		(from2)--;
		(to1)++;
	}
}

void calc_path(instance* inst, const double* x, int* path) {
    int node_before = -1; int node = 0;
    path[0] = node;
    for (int i = 1; i < inst->nnodes; i++) { //edges

        for (int node_after = 0; node_after < inst->nnodes; node_after++) {
            if (node == node_after) continue;
            if (node_before == node_after) continue;

            if (x[cplex_pos_base(inst, node, node_after)] > 0.5) {

                node_before = node;
                node = node_after;
                path[i] = node;
                break;
            }
        }
    }
}

void best_edges_two_opt(instance* inst, int* path, int* from1, int* to1, int* from2, int* to2, double* delta) {
    // To ensure that a swap does not generate any separate components when considering the i-th edge
    // (i-1) and (i+1) must not be chosen.
    for (int i = 0; i < inst->nnodes - 2; i++) {
        for (int j = i + 2; j < inst->nnodes; j++) {
            if ((j + 1) % inst->nnodes == i) continue; // Skip if the next edge is i

            double cur_cost = dist(inst, path[i], path[(i + 1) % inst->nnodes])
                              + dist(inst, path[j], path[(j + 1) % inst->nnodes]);
            double cross_cost = dist(inst, path[i], path[j])
                                + dist(inst, path[(i + 1) % inst->nnodes], path[(j + 1) % inst->nnodes]);
            if (cross_cost - cur_cost < delta[0]) {
                from1[0] = i;
                to1[0] = (i + 1) % inst->nnodes;
                from2[0] = j;
                to2[0] = (j + 1) % inst->nnodes;
                delta[0] = cross_cost - cur_cost;
            }
        }
    }
}

int two_opt(instance* inst, double* x) {
    // Retrieve the path from the solution
	int* path = (int*)calloc(inst->nnodes, sizeof(int));
	calc_path(inst, x, path);
	// While an improvement is found
	double delta = 0.0;
	do {
	    // Find the best possible edge swap which improves the cost
		delta = 0.0;
		int from1 = -1; int to1 = -1; int from2 = -1; int to2 = -1;
		best_edges_two_opt(inst, path, &from1, &to1, &from2, &to2, &delta);

        // If there is improvement apply the swap and update path
		if (delta < 0) cross_edges(inst, x, path, from1, to1, from2, to2);
	} while (delta < 0);

	free(path);
	return 0;
}

void kick_two_opt(instance* inst, double* x) {
    // Retrieve the path from the solution
	int* path = (int*)calloc(inst->nnodes, sizeof(int));
	calc_path(inst, x, path);
    // For each edge two more are forbidden, therefore three total edges are forbidden (exluding the last three)
    int FORBIDDEN_EDGES = 3;
	int* forbidden_nodes = (int*)malloc(sizeof(int) * FORBIDDEN_EDGES);
	for (int i = 0; i < FORBIDDEN_EDGES; i++) forbidden_nodes[i] = -1;
    // Get a random edge
	int e1 = random_at_most(inst->nnodes-1);
	forbidden_nodes[0] = e1;
	forbidden_nodes[1] = (e1 + 1) % inst->nnodes;
	forbidden_nodes[2] = (e1 - 1) % inst->nnodes;
	int e2, loop;
    // Get another random edge different than the previous one and adjacents
	do {
		loop = 0;
		e2 = random_at_most(inst->nnodes-1);
		for (int i = 0; i < 3; i++)
			if (forbidden_nodes[i] == e2) loop = 1;
	} while (loop);

	int from1, to1, from2, to2;
	from1 = e1; to1 = (e1 + 1) % inst->nnodes;
	from2 = e2; to2 = (e2 + 1) % inst->nnodes;
	cross_edges(inst, x, path, from1, to1, from2, to2);

	free(forbidden_nodes);
	free(path);
}

void kick_three_opt(instance* inst, double* x) {
    // Retrieve the path from the solution
	int* path = (int*)calloc(inst->nnodes, sizeof(int));
	calc_path(inst, x, path);
	// For each edge two more are forbidden, therefore six total edges are forbidden (exluding the last three)
	int FORBIDDEN_EDGES = 6;
	int* forbidden_edges = (int*)malloc(sizeof(int) * FORBIDDEN_EDGES);
	for (int i = 0; i < FORBIDDEN_EDGES; i++) forbidden_edges[i] = -1;
	// Get a random edge
	int e1 = random_at_most(inst->nnodes-1);
	forbidden_edges[0] = e1;
	forbidden_edges[1] = (e1 + 1) % inst->nnodes;
	forbidden_edges[2] = (e1 - 1) % inst->nnodes;
	int e2, e3;
	int loop;
	// Get another random edge different than the previous one and adjacents
	do {
		loop = 0;
		e2 = random_at_most(inst->nnodes-1);
		for (int i = 0; i < FORBIDDEN_EDGES; i++)
			if (forbidden_edges[i] == e2) loop = 1;
	} while (loop);
	forbidden_edges[3] = e2;
	forbidden_edges[4] = (e2 + 1) % inst->nnodes;
	forbidden_edges[5] = (e2 - 1) % inst->nnodes;
    // Get another random edge different than the previous ones and adjacents
	do {
		loop = 0;
		e3 = random_at_most(inst->nnodes-1);
		for (int i = 0; i < FORBIDDEN_EDGES; i++)
			if (forbidden_edges[i] == e3) loop = 1;
	} while (loop);

	// Sort the nodes such that the swap are easier to apply, i.e. given 1--2--3, if 1 and 3 are chosen the reverse of
	// elements inbetween requires more checks, this way 1--2 and 2--3 reverse the elements inbetween both of them
	int order_position[3]; order_position[0] = e1; order_position[1] = e2; order_position[2] = e3;
	for (int i = 0; i < 3; i++)	{
		for (int j = 0; j < 3; j++){
			if (order_position[j] > order_position[i]){
				int tmp = order_position[i];         
				order_position[i] = order_position[j];           
				order_position[j] = tmp;          
			}
		}
	}
	e1 = order_position[0]; e2 = order_position[1]; e3 = order_position[2];
	int from1, to1, from2, to2, from3, to3;
	from1 = e1; to1 = (e1 + 1) % inst->nnodes; 
	from2 = e2; to2 = (e2 + 1) % inst->nnodes; 
	from3 = e3; to3 = (e3 + 1) % inst->nnodes;

	// Apply the swap as two two opt swaps
	cross_edges(inst, x, path, from1, to1, from2, to2);	
	cross_edges(inst, x, path, from2, to2, from3, to3);

	free(forbidden_edges);
	free(path);
}


// Simulated Annealing

void simulated_annealing(instance* inst, double* x) {
    srand(time(NULL));
    int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    for (int i = 0; i < inst->nnodes; i++) comp[i] = i;
    for (int i = 0; i < 1000000; i++) { // Shuffle one million edges
        int p1 = random_at_most(inst->nnodes-1);
        int p2 = random_at_most(inst->nnodes-1);
        int dd = comp[p1]; comp[p1] = comp[p2]; comp[p2] = dd;
    }
    for (int i = 0; i < inst->nnodes; i++) {
        x[cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes])] = 1.0;
    }
    free(comp);
    
    
	int xdim = inst->nnodes*(inst->nnodes - 1) / 2;
	double* x_next = (double*)malloc(sizeof(double)*xdim);
	for (int i = 0; i < xdim; i++) x_next[i] = x[i];

	double ALPHA = 0.00001;
	int ITERS = 110000;
	double costx; double costxnext;
	for (int i = 0; i < ITERS; i++) {
	    // Apply a random two opt kick
		kick_two_opt(inst, x_next);

		costx = get_cost_from_x_symm(inst, x);

		costxnext = get_cost_from_x_symm(inst, x_next);
		// Apply the ALPHA decrease
		costxnext *= ALPHA;
		if (costxnext - costx < 0) {
			for (int j = 0; j < xdim; j++) x[j] = x_next[j];
		} else {
			for (int j = 0; j < xdim; j++) x_next[j] = x[j];
		}
		// Update ALPHA
		if (ALPHA < 1.0) ALPHA += 0.00001;
	}

	free(x_next);
}

void simulated_annealing2(instance* inst, double* x) {
    srand(time(NULL));
    int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    for (int i = 0; i < inst->nnodes; i++) comp[i] = i;
    for (int i = 0; i < 1000000; i++) { // Shuffle one million edges
        int p1 = random_at_most(inst->nnodes-1);
        int p2 = random_at_most(inst->nnodes-1);
        int dd = comp[p1]; comp[p1] = comp[p2]; comp[p2] = dd;
    }
    for (int i = 0; i < inst->nnodes; i++) {
        x[cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes])] = 1.0;
    }
    free(comp);


    int xdim = inst->nnodes*(inst->nnodes - 1) / 2;
    double* x_next = (double*)malloc(sizeof(double)*xdim);
    for (int i = 0; i < xdim; i++) x_next[i] = x[i];

    // Normalization term
    double k = 2 * get_cost_from_x_symm(inst, x) / inst->nnodes;

    //double ALPHA = 0.001;
    double ALPHA = 0.999;
    double TEMPERATURE = 1000;
    int ITERS = 100000;
    double costx; double costxnext;
    for (int i = 0; i < ITERS; i++) {
        // Apply a random two opt kick
        kick_two_opt(inst, x_next);

        costx = get_cost_from_x_symm(inst, x);

        costxnext = get_cost_from_x_symm(inst, x_next);
        // Apply the ALPHA decrease
        //costxnext *= ALPHA;
        double delta = (costxnext - costx) / k;
        double distribution = exp(- delta / TEMPERATURE);
        double prob = (double) rand() / RAND_MAX;
        //printf("%f,%f,%f,%f\n", k, delta, distribution, prob);
        //if (costxnext - costx < 0) {
        if ( (costxnext - costx <= 0) || (prob <= distribution) ) {
            for (int j = 0; j < xdim; j++) x[j] = x_next[j];
            //printf("%d,%f,%f,%f,%f,%f\n", i, k, TEMPERATURE, delta, distribution, costxnext);
        } else {
            for (int j = 0; j < xdim; j++) x_next[j] = x[j];
            //printf("%d,%f,%f,%f,%f,%f\n", i, k, TEMPERATURE, delta, distribution, costx);
        }
        // Update ALPHA
        //if (ALPHA < 1.0) ALPHA += 0.001;
        TEMPERATURE *= ALPHA;
    }

    free(x_next);
}
// Hard-Fixing and utilies

void collapse_components(instance* inst, int* comp, double* x) {

    set_components(inst, comp, x);

    while(1) {
        int fixed_node = 0; // First node, fixed arbitrarily
        int cur_node = 1; // Second node

        // Increment cur_node until it belongs to a different component than fixed_node, or break if it reached the end
        while (cur_node < inst->nnodes && comp[cur_node] == comp[fixed_node]) cur_node++;
        if (cur_node == inst->nnodes) break;
        // Take the first existing edge in which fixed_node takes part in
        int edge_pos1 = -1, edge_pos2 = -1, fixed_comp_node = -1, cur_comp_node = -1;
        for (int j = 0; j < inst->nnodes; j++) {
            if(fixed_node == j) continue;
            edge_pos1 = cplex_pos_base(inst, fixed_node, j);
            fixed_comp_node = j;
            if(x[edge_pos1] > 0.5) break;
        }
        // Do the same for cur_node
        for (int k = 0; k < inst->nnodes; k++) {
            if(cur_node == k) continue;
            edge_pos2 = cplex_pos_base(inst, cur_node, k);
            cur_comp_node = k;
            if(x[edge_pos2] > 0.5) break;
        }

        // Patch together arbitrarily
        x[edge_pos1] = 0.0; x[edge_pos2] = 0.0;
        int new_edge_pos1 = cplex_pos_base(inst, fixed_node, cur_node);
        int new_edge_pos2 = cplex_pos_base(inst, fixed_comp_node, cur_comp_node);
        x[new_edge_pos1] = 1.0; x[new_edge_pos2] = 1.0;

        // All the nodes in the second component now belong to the first component
        for (int j = 0; j < inst->nnodes; j++)
            if (comp[j] == comp[cur_node]) comp[j] = comp[fixed_node];
    }
}

int hcloop(instance* inst, CPXENVptr env, CPXLPptr lp) {

	srand(time(NULL));
    mip_timelimit(env, 60.0);

	int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    for (int i = 0; i < inst->nnodes; i++) comp[i] = i;

    int* vartour = (int*)calloc(inst->nnodes, sizeof(int)); int indexvartour = 0;
    double* valtour = (double*)calloc(inst->nnodes, sizeof(double));
    for(int i=0; i<inst->nnodes; i++) valtour[i] = 1.0;
    int zeroInt = 0;

	double* x = (double*)calloc(inst->nnodes*(inst->nnodes - 1) / 2, sizeof(double));
	double b = 0.0;	char sense = 'E';
	char **cname = (char **)calloc(1, sizeof(char *));
	cname[0] = (char *)calloc(100, sizeof(char));
	sprintf(cname[0], "Heuristic_constraint");

	if (CPXnewrows(env, lp, 1, &b, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

	int heuristicRow = CPXgetnumrows(env, lp) - 1;

	for (int i = 0; i < 1000000; i++) { // Shuffle one million edges
		int p1 = random_at_most(inst->nnodes-1);
		int p2 = random_at_most(inst->nnodes-1);
		int dd = comp[p1]; comp[p1] = comp[p2]; comp[p2] = dd;
	}

	// Fix edges on the constraint with probability prob
	double prob = 0.6; double count = 0.0;
	for (int i = 0; i < inst->nnodes; i++) {
		double d = (double)rand();
		d /= RAND_MAX;
		if (d < prob) {
			count = count + 1;
			CPXchgcoef(env, lp, heuristicRow, cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes]), 1.0);
			x[cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes])] = 1.0;
		}
	}
	CPXchgrhs(env, lp, 1, &heuristicRow, &count);

	int iter = 0; int max_iter = 10;
	int res = 1;
	double objvalue = ULONG_MAX;
	double objvalueincumbent = ULONG_MAX;
    double sol = ULONG_MAX;

    sprintf(cname[0], "mipstart");


    while (iter < max_iter) {
        if(VERBOSE>=200) printf("\nIteration: %d \n", iter+1);
        res = cloop(inst, env, lp);
        if (mip_solved_to_optimality(env, lp)) {
            CPXgetobjval(env, lp, &sol);
            iter++;
            if (sol < objvalue) {
                if(VERBOSE>=100) printf("s:%f\n", sol);
                fflush(stdout);
                objvalue = sol; objvalueincumbent = sol;
                iter = 0;
                CPXgetx(env, lp, x, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1);
            }
        } else {
            if(VERBOSE>=200) printf("Reached Timelimit\n");
            CPXgetsolnpoolobjval(env, lp, 0, &sol);
            iter++;
            if(sol < objvalueincumbent && sol>0){
                if(VERBOSE>=200) printf("Incumbent solution found: %f\n", sol);
                objvalueincumbent = sol;
                iter = 0;
                CPXgetsolnpoolx(env, lp, 0, x, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1);
                collapse_components(inst, comp, x);
                if(VERBOSE>=100) printf("s:%f\n", get_cost_from_x_symm(inst, x));
                fflush(stdout);
            }
        }

        // Fix edges with probability prob
        if (iter < max_iter) {
            if (iter > max_iter/2 && prob > 0.31) {
                prob = 0.3;
                if(VERBOSE>=200) printf("\nChanging probability to: %f\n", prob);
            }
            count = 0.0;
            for (int i = 0; i < inst->nnodes; i++) {
                for (int j = i + 1; j < inst->nnodes; j++) {
                    int pos = cplex_pos_base(inst, i, j);
                    if (x[pos] > 0.5) {
                        vartour[indexvartour] = pos; indexvartour = (indexvartour+1)%inst->nnodes;
                        double d = (double)rand();
                        d /= RAND_MAX;
                        if (d < prob) { // Edge must be used
                            count = count + 1;
                            CPXchgcoef(env, lp, heuristicRow, pos, 1.0);
                        } else { // Edge is free
                            CPXchgcoef(env, lp, heuristicRow, pos, 0.0);
                        }
                    }
                }
            }
            CPXchgrhs(env, lp, 1, &heuristicRow, &count);
            CPXaddmipstarts(env, lp, 1, inst->nnodes, &zeroInt, vartour, valtour, CPX_MIPSTART_AUTO, cname);
        }
    }
    CPXgetobjval(env, lp, &objvalue);
    if(VERBOSE>=200) printf("\nFinal Solution: %f\n", objvalue);

	free(cname[0]);
	free(cname);
	free(comp);
	free(vartour);
    free(valtour);
	free(x);
	return res;
}


// Local branching

int localbranching(instance* inst, CPXENVptr env, CPXLPptr lp) {

	srand(time(NULL));
	double timelimit = 60.0;
	mip_timelimit(env, timelimit);

	int* comp = (int*)calloc(inst->nnodes, sizeof(int));
    for (int i = 0; i < inst->nnodes; i++) comp[i] = i;

    int* vartour = (int*)calloc(inst->nnodes, sizeof(int)); int indexvartour = 0;
    double* valtour = (double*)calloc(inst->nnodes, sizeof(double));
    for(int i=0; i<inst->nnodes; i++) valtour[i] = 1.0;
    int zeroInt = 0;

	double* x = (double*)calloc(inst->nnodes*(inst->nnodes - 1) / 2, sizeof(double));
	double b = 0.0;	char sense = 'G';
	char **cname = (char **)calloc(1, sizeof(char *));
	cname[0] = (char *)calloc(100, sizeof(char));
	sprintf(cname[0], "Heuristic_constraint_localbranching");

	if (CPXnewrows(env, lp, 1, &b, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

	int heuristicRow = CPXgetnumrows(env, lp) - 1;

	for (int i = 0; i < 1000000; i++) { // Shuffle one million edges
		int p1 = random_at_most(inst->nnodes-1);
		int p2 = random_at_most(inst->nnodes-1);
		int dd = comp[p1]; comp[p1] = comp[p2]; comp[p2] = dd;
	}

	// Add the chosen edges to the constraint and impose the % of choice
	for (int i = 0; i < inst->nnodes; i++) {
		CPXchgcoef(env, lp, heuristicRow, cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes]), 1.0);
		x[cplex_pos_base(inst, comp[i], comp[(i + 1)%inst->nnodes])] = 1.0;
	}
	double prob = 0.75;
	double rhs = prob*inst->nnodes;
	CPXchgrhs(env, lp, 1, &heuristicRow, &rhs);

	int iter = 0; int max_iter = 3;
	int res = 1;
	double objvalue = ULONG_MAX;
    double objvalueincumbent = ULONG_MAX;
    double sol = ULONG_MAX;

    sprintf(cname[0], "mipstart");

	while (iter < max_iter) {
        if(VERBOSE>=200) printf("\nIterations: %d\n", iter + 1);
        mip_timelimit(env, timelimit);
		res = cloop(inst, env, lp);
		if (mip_solved_to_optimality(env, lp)) {
			CPXgetobjval(env, lp, &sol);
			iter++;
			if (sol < objvalue) {
                if(VERBOSE>=100) printf("s:%f\n", sol);
                fflush(stdout);
                objvalue = sol; objvalueincumbent = sol;
                iter = 0;
                CPXgetx(env, lp, x, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1);
                timelimit = 60.0;

			}
		} else {
            if(VERBOSE>=200) printf("Reached Timelimit\n");
            CPXgetsolnpoolobjval(env, lp, 0, &sol);
            iter++;
            timelimit = 2*timelimit;
            if(sol < objvalueincumbent && sol>0){
                if(VERBOSE>=200) printf("Incumbent solution found: %f\n", sol);
                timelimit = 60.0;
                objvalueincumbent = sol;
                iter = 0;
                CPXgetsolnpoolx(env, lp, 0, x, 0, (inst->nnodes*(inst->nnodes - 1) / 2) - 1);
                collapse_components(inst, comp, x);
                if(VERBOSE>=100) printf("s:%f\n", get_cost_from_x_symm(inst, x));
                fflush(stdout);
            }
		}

		if (iter < max_iter) {
            if (iter > max_iter/2 && prob > 0.41) {
                prob = 0.4;
                if(VERBOSE>=200) printf("\nChanging probability to: %f\n", prob);
                rhs = prob*inst->nnodes;
                CPXchgrhs(env, lp, 1, &heuristicRow, &rhs);
            }
			for (int i = 0; i < inst->nnodes; i++) {
				for (int j = i + 1; j < inst->nnodes; j++) {
					int pos = cplex_pos_base(inst, i, j);
					if (x[pos] > 0.5) {
                        vartour[indexvartour] = pos; indexvartour = (indexvartour+1)%inst->nnodes;
                        CPXchgcoef(env, lp, heuristicRow, pos, 1.0);
					} else { // Edge is free
					    CPXchgcoef(env, lp, heuristicRow, pos, 0.0);
					}					
				}
			}
            CPXaddmipstarts(env, lp, 1, inst->nnodes, &zeroInt, vartour, valtour, CPX_MIPSTART_AUTO, cname);
		}
	}
    CPXgetobjval(env, lp, &objvalue);
	if(VERBOSE>=200) printf("\nFinal Solution: %f\n", objvalue);

	free(cname[0]);
	free(cname);
	free(comp);
    free(vartour);
    free(valtour);
	free(x);
	return res;
}


// GRASP
int grasp(instance* inst, double prob, int neighbours, double* x_star, double* cost) {

    for (int i = 0; i < inst->nnodes*(inst->nnodes - 1) / 2; i++) x_star[i] = 0.0;
    *cost = 0;

    int fixed_node = random_at_most(inst->nnodes-1); int first_node = fixed_node;
    int used_counter = 1;
    int* used_nodes = calloc(inst->nnodes, sizeof(int)); used_nodes[fixed_node] = 1;

    if (neighbours < 0 || neighbours > inst->nnodes) print_error(" neighbours must be greater than zero and lesser than the number of nodes.");
    if (prob < 0.0 || prob > 1.0) print_error(" probability must be greater than zero and lesser than one.");

    while (used_counter < inst->nnodes) {
        double cur_dist = 0;

        double* min_dist = calloc(neighbours, sizeof(double));
        int* min_dist_nodes = calloc(neighbours, sizeof(int));

        for (int j = 0; j < inst->nnodes; j++) {
            if (j == fixed_node || used_nodes[j]) continue; // Skip already visited nodes

            double distance = dist(inst, fixed_node, j);
            // If the current distance is smaller than the largest in the array or the array was not filled yet
            if (distance < min_dist[neighbours - 1] || min_dist[neighbours - 1] == 0.0) {
                // Replace its value with the current distance.
                min_dist[neighbours - 1] = distance; min_dist_nodes[neighbours - 1] = j;
                // Swap values such that [neighbours - 1] always holds the next value to be replaced, which might be
                // zero, if the array was not filled yet, or the biggest distance currently in the array;
                // The index zero holds the smallest distance currently in the array, it is followed by increasing
                // values of distances or zeros if the array was not filled yet.
                for (int t = neighbours - 2; t >= 0; t--)
                    if (min_dist[t] > min_dist[t + 1] || min_dist[t] == 0.0) {
                        double h = min_dist[t + 1];
                        min_dist[t + 1] = min_dist[t];
                        min_dist[t] = h;
                        int k = min_dist_nodes[t + 1];
                        min_dist_nodes[t + 1] = min_dist_nodes[t];
                        min_dist_nodes[t] = k;
                    }
                    else break;
            }
        }


        int min_counter = neighbours;
        for (int j = 0; j < neighbours; j++) {
            if (min_dist[j] == 0.0) min_counter--;
        }

        int chosen_node = -1;
        double p = (double) rand() / RAND_MAX;
        if (p <= prob)  {
            // Use Greedy
            chosen_node = 0;
        } else {
            // Choose randomly
            if (min_counter < neighbours) // To avoid choosing fixed_node if less than neighbours min are found
                p = random_at_most(min_counter-1);
            else
                p = random_at_most(neighbours-1);
            chosen_node = (int) p;
        }

        x_star[cplex_pos_base(inst, fixed_node, min_dist_nodes[chosen_node])] = 1.0;
        *cost += min_dist[chosen_node];
        used_nodes[min_dist_nodes[chosen_node]] = 1;
        used_counter++;
        fixed_node = min_dist_nodes[chosen_node];

        free(min_dist_nodes);
        free(min_dist);
    }
    x_star[cplex_pos_base(inst, fixed_node, first_node)] = 1.0;
    *cost += dist(inst, fixed_node, first_node);
    free(used_nodes);

    return 0;
}


// Switch

int solve_model(instance* inst, CPXENVptr env, CPXLPptr lp) {
	switch (inst->model) {
	case MODEL_BASE:
	case MODEL_BASE_LOOP:	
		return loop(inst, env, lp);
		break;
	case MODEL_BASE_CALLBACK_LOOP:
		return cloop(inst, env, lp);
	case MODEL_BASE_HARD_FIXING:
		return hcloop(inst, env, lp);
	case MODEL_BASE_LOCAL_BRANCHING:
		return localbranching(inst, env, lp);
	}
	return CPXmipopt(env, lp);
}

int exec_solver(instance* inst, CPXENVptr env, CPXLPptr lp) {

	switch (inst->model) {
	case MODEL_BASE:
    case MODEL_BASE_LOOP:
    case MODEL_BASE_CALLBACK_LOOP:
	case MODEL_BASE_HARD_FIXING:
	case MODEL_BASE_LOCAL_BRANCHING:
		build_model_base(inst, env, lp);					// Build the model
		//CPXwriteprob(env, lp, "TSP_model_base.lp", NULL);	// Write the model on file
		break;
	case MODEL_COMPACT_MTZ:
		build_model_compact_MTZ(inst, env, lp);
		//CPXwriteprob(env, lp, "TSP_model_compact_MTZ.lp", NULL);
		break;
	case MODEL_COMPACT_CUSTOM:
		build_model_compact_custom(inst, env, lp);
		//CPXwriteprob(env, lp, "TSP_model_compact_custom.lp", NULL);
		break;
	case MODEL_COMPACT_F1:
		build_model_compact_F1(inst, env, lp);
		//CPXwriteprob(env, lp, "TSP_model_compact_F1.lp", NULL);
		break;
	default:
	    return 0;
	}

	if(inst->solve) return solve_model(inst, env, lp);
	return 0;
}

// Utilities

void free_instance(instance *inst) { free(inst->xcoord); free(inst->ycoord); }