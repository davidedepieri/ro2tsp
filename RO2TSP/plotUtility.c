#include "plotUtility.h"

int gpc_plot_xy_custom(h_GPC_Plot *plotHandle, const ComplexRect_s *pData, const int graphLength, const char *plotType,
                       const char *firstpColour, const char *pColour, const enum gpcNewAddGraphMode addMode,
                       const double x_range, const double y_range, const double x_min, const double y_min) {

	// Radius of the "points"
	double maximum = x_range;
	if (x_range < y_range) maximum = y_range;
	double ray_p = maximum / 200;
	fprintf(plotHandle->pipe, "set style fill transparent solid 0.6 noborder\n");

	if (addMode == GPC_NEW) {

		if (plotHandle->scalingMode == GPC_AUTO_SCALE) {		// Set the X and Y axis scaling
			fprintf(plotHandle->pipe, "set autoscale xy\n");	// Auto-scale Y axis
		}
		else {
			fprintf(plotHandle->pipe, "set xrange[-%1.6le:%1.6le]\n", -x_min, x_range+ 4 * ray_p);
			fprintf(plotHandle->pipe, "set yrange[-%1.6le:%1.6le]\n", -y_min, y_range+4 * ray_p);
		}

		if (plotHandle->numberOfGraphs > 0) {
			fprintf(plotHandle->pipe, "unset multiplot\n");		// If there is an existing multiplot then close it
		}
		fprintf(plotHandle->pipe, "set multiplot\n");

		plotHandle->numberOfGraphs = 1;							// Reset the number of plots
	} else {	// GPC_ADD

		plotHandle->numberOfGraphs++;							// Increment the number of graphs
	}


	fprintf(plotHandle->pipe, "plot '-' using 1:2:(%f) with %s lt rgb \"%s\"\n", ray_p, plotType, firstpColour);
	fprintf(plotHandle->pipe, "%1.6le %1.6le\n", pData[0].real, pData[0].imag);
	fprintf(plotHandle->pipe, "e\n");
	fprintf(plotHandle->pipe, "plot '-' using 1:2:(%f) with %s lt rgb \"%s\"\n", ray_p, plotType, pColour);

	// Copy the data to gnuplot
	for (int i = 1; i < graphLength; i++) { fprintf(plotHandle->pipe, "%1.6le %1.6le\n", pData[i].real, pData[i].imag);	}

	fprintf(plotHandle->pipe, "e\n");	// End of dataset
	for (int i = 0; i < graphLength; i++) fprintf(plotHandle->pipe, "set label '%d' at %f,%f size 2\n", i + 1, pData[i].real, pData[i].imag + 3*ray_p);
	fflush(plotHandle->pipe);			// Flush the pipe

#if GPC_DEBUG
	mssleep(100);	// Slow down output so that pipe doesn't overflow when logging results
#endif

	return (GPC_NO_ERROR);
}



void gpc_setup_plot_line(h_GPC_Plot *plotHandle) {
	fprintf(plotHandle->pipe, "set style noborder\n");
	fprintf(plotHandle->pipe, "plot '-' with %s lt rgb \"%s\"\n", "lines", "midnight-blue");
}
void gpc_plot_line(h_GPC_Plot *plotHandle, const ComplexRect_s *pData) {
	for (int i = 0; i < 2; i++) { fprintf(plotHandle->pipe, "%1.6le %1.6le\n", pData[i].real, pData[i].imag); }
	fprintf(plotHandle->pipe, "\n");
}
void gpc_close_plot_line(h_GPC_Plot *plotHandle) {
	fprintf(plotHandle->pipe, "e\n");
	fflush(plotHandle->pipe);
}

h_GPC_Plot* plot_symm_sol_heu(instance* inst, CPXENVptr env, CPXLPptr lp, double* x) {

	h_GPC_Plot *plotter;
	plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

	ComplexRect_s* data = (ComplexRect_s*)calloc(inst->nnodes, sizeof(ComplexRect_s));
	ComplexRect_s edge[2];
	

	for (int i = 0; i < inst->nnodes; i++) { data[i].real = inst->xcoord[i]; data[i].imag = inst->ycoord[i]; }

	// Plot the vertices
	gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
		inst->max_y_val, inst->min_x_val, inst->min_y_val);

	// Plot the edges
	gpc_setup_plot_line(plotter);
	for (int i = 0; i < inst->nnodes; i++) {
		int count = 0;
		edge[0].real = inst->xcoord[i];
		edge[0].imag = inst->ycoord[i];

		for (int j = i + 1; j < inst->nnodes; j++) {

			int cxpos = cplex_pos_base(inst, i, j);

			if (x[cxpos] > 0.5) {
				edge[1].real = inst->xcoord[j];
				edge[1].imag = inst->ycoord[j];
				gpc_plot_line(plotter, edge);
				if (++count == 2) break;
			}
		}
	}

	gpc_close_plot_line(plotter);
	free(data);

	return plotter;
}

h_GPC_Plot* plot_symm_sol(instance* inst, CPXENVptr env, CPXLPptr lp) {

	h_GPC_Plot *plotter;
	plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

	ComplexRect_s* data = (ComplexRect_s*) calloc(inst->nnodes, sizeof(ComplexRect_s));
	ComplexRect_s edge[2];
	double* x = (double*) calloc(1, sizeof(double));

	for (int i = 0; i < inst->nnodes; i++) { data[i].real = inst->xcoord[i]; data[i].imag = inst->ycoord[i]; }

	// Plot the vertices
    gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
                       inst->max_y_val, inst->min_x_val, inst->min_y_val);
	
	// Plot the edges
	gpc_setup_plot_line(plotter);
	for (int i = 0; i < inst->nnodes; i++) {
		int count = 0;
		edge[0].real = inst->xcoord[i];
		edge[0].imag = inst->ycoord[i];

		for (int j = i+1; j < inst->nnodes; j++) {

			int cxpos = cplex_pos_base(inst, i, j);
			CPXgetx(env, lp, x, cxpos, cxpos);

			if (x[0] > 0.5) {
				edge[1].real = inst->xcoord[j];
				edge[1].imag = inst->ycoord[j];
				gpc_plot_line(plotter, edge);
				if (++count == 2) break;
			}
		}
	}

	gpc_close_plot_line(plotter);
	free(x);
	free(data);

	return plotter;
}

h_GPC_Plot* plot_asymm_sol(instance* inst, CPXENVptr env, CPXLPptr lp) {

	h_GPC_Plot *plotter;
	plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

	ComplexRect_s* data = (ComplexRect_s*)calloc(inst->nnodes, sizeof(ComplexRect_s));
	ComplexRect_s edge[2];
	double* x = (double*)calloc(1, sizeof(double));

	for (int i = 0; i < inst->nnodes; i++) { data[i].real = inst->xcoord[i]; data[i].imag = inst->ycoord[i]; }

	// Plot the vertices
    gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
                       inst->max_y_val, inst->min_x_val, inst->min_y_val);

	// Plot the edges
	gpc_setup_plot_line(plotter);
	for (int i = 0; i < inst->nnodes; i++) {
		int count = 0;
		edge[0].real = inst->xcoord[i];
		edge[0].imag = inst->ycoord[i];

		for (int j = 0; j < inst->nnodes; j++) {
			if (i == j) continue;
			int cxpos = cplex_pos_compact_MTZ(inst, i, j);
			CPXgetx(env, lp, x, cxpos, cxpos);

			if (x[0] > 0.5) {
				//printf("x(%d,%d)\n", i + 1, j + 1);
				edge[1].real = inst->xcoord[j];
				edge[1].imag = inst->ycoord[j];
				gpc_plot_line(plotter, edge);
				if (++count == 2) break;
			}
		}
	}

	gpc_close_plot_line(plotter);
	free(x);
	free(data);

	return plotter;
}

h_GPC_Plot* plot_sol(instance* inst, const double* x_star) {

    h_GPC_Plot *plotter;
    plotter = gpc_init_xy("Best Solution", "X Coordinates", "Y Coordinates", 1500, GPC_KEY_DISABLE);

    ComplexRect_s* data = (ComplexRect_s*) calloc(inst->nnodes, sizeof(ComplexRect_s));
    ComplexRect_s edge[2];

    for (int i = 0; i < inst->nnodes; i++) { data[i].real = inst->xcoord[i]; data[i].imag = inst->ycoord[i]; }

    // Plot the vertices
    gpc_plot_xy_custom(plotter, data, inst->nnodes, "circles", "forest-green", "red", GPC_NEW, inst->max_x_val,
                       inst->max_y_val, inst->min_x_val, inst->min_y_val);

    // Plot the edges
    gpc_setup_plot_line(plotter);
    for (int i = 0; i < inst->nnodes; i++) {
        int count = 0;
        edge[0].real = inst->xcoord[i];
        edge[0].imag = inst->ycoord[i];

        for (int j = i+1; j < inst->nnodes; j++) {

            int cxpos = cplex_pos_base(inst, i, j);

            if (x_star[cxpos] > 0.5) {
                edge[1].real = inst->xcoord[j];
                edge[1].imag = inst->ycoord[j];
                gpc_plot_line(plotter, edge);
                if (++count == 2) break;
            }
        }
    }

    gpc_close_plot_line(plotter);
    free(data);

    return plotter;

}

h_GPC_Plot* plot_cplex_sol(instance* inst, CPXENVptr env, CPXLPptr lp) {

	switch (inst->model) {
	case MODEL_BASE:
    case MODEL_BASE_LOOP:
    case MODEL_BASE_CALLBACK_LOOP:
	case MODEL_BASE_HARD_FIXING:
	case MODEL_BASE_LOCAL_BRANCHING:
		return plot_symm_sol(inst, env, lp);
	case MODEL_COMPACT_MTZ:
		return plot_asymm_sol(inst, env, lp);
	case MODEL_COMPACT_CUSTOM:
		return plot_symm_sol(inst, env, lp);
	case MODEL_COMPACT_F1:
		return plot_asymm_sol(inst, env, lp);
	}
	return NULL;
}