#ifndef PLOTUTILITY_H
#define PLOTUTILITY_H

#include <stdio.h>
#include <gnuplot_c.h>
#include "tsp.h"

/** Initializes the model in CPLEX and stores it locally then solves the optimization problem.
*
* @param plotHandle Plot handle.
* @param pData Dataset pointer.
* @param graphLength Dataset length.
* @param pDataName Dataset title.
* @param plotType Plot type - "lines", "points", "impulses", "linespoints", "circles".
* @param firstpColour Colour of the first dataset point.
* @param pColour Colour of the dataset points.
* @param addMode Add / new mode.
* @param x_range Range of the x axis.
* @param y_range Range of the y axis.
* @return zero if successful.
*/
int gpc_plot_xy_custom(h_GPC_Plot *plotHandle, const ComplexRect_s *pData, int graphLength, const char *plotType,
                       const char *firstpColour, const char *pColour, enum gpcNewAddGraphMode addMode,
                       double x_range, double y_range, double x_min, double y_min);

/** Initializes the model in CPLEX and stores it locally then solves the optimization problem.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
* @return zero if successful and nonzero if an error occurs.
*/
void gpc_setup_plot_line(h_GPC_Plot *plotHandle);
void gpc_plot_line(h_GPC_Plot *plotHandle, const ComplexRect_s *pData);
void gpc_close_plot_line(h_GPC_Plot *plotHandle);

/** Plots the vertices and the TSP solution.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
* @return Plot handle.
*/
h_GPC_Plot* plot_cplex_sol(instance* inst, CPXENVptr env, CPXLPptr lp);
h_GPC_Plot* plot_symm_sol(instance* inst, CPXENVptr env, CPXLPptr lp);
h_GPC_Plot* plot_asymm_sol(instance* inst, CPXENVptr env, CPXLPptr lp);
h_GPC_Plot* plot_symm_sol_heu(instance* inst, CPXENVptr env, CPXLPptr lp, double* x);

/**
 * Plots the vertices and the specified TSP solution
 * @param inst Pointer to an instance of the "instance" struct.
 * @param x_star Solution vector.
 * @return Plot handle.
 */
h_GPC_Plot* plot_sol(instance* inst, const double* x_star);

#endif
