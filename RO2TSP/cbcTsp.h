#ifndef CBC_TSP_H
#define CBC_TSP_H

#include "tsp.h"
#include "plotUtility.h"
#include <Cbc_C_Interface.h>
#include "unistd.h"

/** Initializes the model in CBC and stores it locally then solves the optimization problem.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param mod CBC Coin-Or Model.
* @return zero if successful and nonzero if an error occurs.
*/
int cbc_exec_solver(instance* inst, Cbc_Model **mod);
int cbc_solve_model(instance* inst, Cbc_Model **mod);
int cbc_loop(instance* inst, Cbc_Model **mod);
int cbc_cloop(instance* inst, Cbc_Model **mod);

/** Builds the model in CBC by inserting all the variables and constraints.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param mod CBC Coin-Or Model.
*/
void cbc_build_model_base(instance* inst, Cbc_Model *mod);
void cbc_build_model_compact_MTZ(instance* inst, Cbc_Model *mod);
void cbc_build_model_compact_custom(instance* inst, Cbc_Model *mod);
void cbc_build_model_compact_F1(instance* inst, Cbc_Model *mod);

/** Plots the vertices and the TSP solution.
*
* @param inst Pointer to an instance of the "instance" struct.
* @param env CPLEX Environment struct.
* @param lp CPLEX Linear Problem struct.
* @return Plot handle.
*/
h_GPC_Plot* plot_cbc_sol(instance* inst, Cbc_Model *mod);
h_GPC_Plot* cbc_plot_symm_sol(instance* inst, Cbc_Model *mod);
h_GPC_Plot* cbc_plot_asymm_sol(instance* inst, Cbc_Model *mod);

#endif // !CBC_TSP_H