#include <time.h>
#include "inout.h"
#include "plotUtility.h"
#include "cbcTsp.h"


int main(int argc, char **argv) {
	if (argc <= 1) { printf("Usage: %s -h or --help for help\n", argv[0]); exit(1); }
	
	instance inst;	

	parse_command_line(argc, argv, &inst);

	read_input(&inst);

	// Initialize CPLEX env and lp structs
	int error;
	CPXENVptr env = CPXopenCPLEX(&error);
	CPXLPptr lp = CPXcreateprob(env, &error, "TSP");

	// Initialize CBC model
	Cbc_Model *mod;
	mod = Cbc_newModel();
    Cbc_setLogLevel(mod, 0);
	Cbc_setProblemName(mod, "TSP");


    // Try to solve the optimization problem
	h_GPC_Plot* plotter = NULL;
	time_t t1 = time(NULL);
	switch (inst.solver) {
        case SOLVER_CPLEX:
            if (exec_solver(&inst, env, lp)) print_error(" problem was NOT solved successfully");
            time_t t2 = time(NULL);
            if(VERBOSE>=100) {
                //printf("... CPLEX: TSP problem solved in %li sec.s\n", t2 - t1);

                double sol; CPXgetobjval(env, lp, &sol);
                //printf("Best sol found: %f\n", sol);
                printf("s:%f\n", sol); // costo soluzione
                printf("t:%li\n", t2 - t1); // tempo di risoluzione
                fflush(stdout);
            }
            plotter = plot_cplex_sol(&inst, env, lp);

            break;
        case SOLVER_CBC:
            cbc_exec_solver(&inst, &mod);
            time_t t3 = time(NULL);
            double sol = Cbc_getObjValue(mod);
            if (VERBOSE >= 100) {// printf("... CBC: TSP problem solved in %li sec.s\n", t3 - t1);
                printf("s:%f\n", sol);
                printf("t:%li\n", t3 - t1);
                fflush(stdout);
            }
            //const double* x = Cbc_getColSolution(mod);
            //for(int i=0; i<(inst.nnodes+1)*inst.nnodes/2; i++) printf("%f ",x[i]);
            printf("\n");
            plotter = plot_cbc_sol(&inst, mod);
            break;
        /*case SOLVER_COMBO:
            if (exec_solver(&inst, env, lp)) print_error(" problem cplex was NOT solved successfully");
            time_t t4 = time(NULL);
            cbc_exec_solver(&inst, &mod);
            time_t t5 = time(NULL);
            if (VERBOSE >= 100) printf("... CPLEX: TSP problem solved in %li sec.s\n", t4 - t1);
            if (VERBOSE >= 100) printf("... CBC: TSP problem solved in %li sec.s\n", t5 - t4);
            plotter = plot_cplex_sol(&inst, env, lp);
            break;*/
        case SOLVER_NEIGHBORHOOD_SEARCH:
            {
                double* x = getNearestNeihgborhoodPath(&inst, 0, 4);
                for (int i = 0; i < 5; i++) {
                    kick_three_opt(&inst, x);
                    kick_three_opt(&inst, x);
                    kick_three_opt(&inst, x);
                    //printf("sol after kick: %f\n", get_cost_from_x_symm(&inst, x));
                    two_opt(&inst, x);
                    //printf("best sol %d interaction: %f\n", i+1, get_cost_from_x_symm(&inst, x));
                }
                time_t t6 = time(NULL);
                //if (VERBOSE >= 100) printf("... SOLVER_NEIGHBORHOOD_SEARCH: TSP problem solved in %li sec.s\n", t6 - t1);

                printf("s:%f\n", get_cost_from_x_symm(&inst, x));
                printf("t:%li\n", t6 - t1);
                fflush(stdout);
                plotter = plot_symm_sol_heu(&inst, env, lp, x);
                /*fprintf(plotter->pipe, "clear\n");
                gpc_close_plot_line(plotter);*/
                free(x);
                break;
            }
        case SOLVER_SIMULATED_ANNEALING:
        {
            double* x = (double*)calloc(inst.nnodes*(inst.nnodes - 1) / 2, sizeof(double));

            simulated_annealing2(&inst, x);
            time_t t7 = time(NULL);
            //if (VERBOSE >= 100) printf("... SOLVER_SIMULATED_ANNEALING: TSP problem solved in %li sec.s\n", t7 - t1);

            printf("s:%f\n", get_cost_from_x_symm(&inst, x));
            printf("t:%li\n", t7 - t1);
            fflush(stdout);
            plotter = plot_symm_sol_heu(&inst, env, lp, x);
            free(x);
            break;
        }
        case SOLVER_GRASP:
        {
            int GRASP_ITER = 50;
            double* cur_x = calloc(inst.nnodes*(inst.nnodes - 1) / 2, sizeof(double));
            double cur_cost = 0;
            double* best_x = calloc(inst.nnodes*(inst.nnodes - 1) / 2, sizeof(double));
            double best_cost = DBL_MAX;

            srand(time(NULL));
            for (int i = 0; i < GRASP_ITER; i++) {
                grasp(&inst, 0.5, 4, cur_x, &cur_cost);
                //printf("id=%d, cost: %f\n", i, cur_cost);
                if (cur_cost < best_cost) {
                    for (int j = 0; j < inst.nnodes*(inst.nnodes - 1) / 2; j++)
                        best_x[j] = cur_x[j];
                    best_cost = cur_cost;
                }
            }
            time_t t8 = time(NULL);
            //if (VERBOSE >= 100) printf("... SOLVER_GRASP: TSP problem solved in %li sec.s\n", t8 - t1);

            printf("s:%f\n", best_cost);
            printf("t:%li\n", t8 - t1);
            fflush(stdout);
            plotter = plot_sol(&inst, best_x);

            free(cur_x);
            free(best_x);
        }
	}

	if (!inst.solve) { printf("Problem was NOT solved.\n"); exit(0); }

	// Free the allocated space and close the plots
	Cbc_deleteModel(mod);
	CPXfreeprob(env, &lp);
	CPXcloseCPLEX(&env);
	free_instance(&inst);

	//printf("Waiting for return... "); getchar();

	if (plotter != NULL) gpc_close(plotter);

	exit(0);
}