#include "util.h"

void print_error(const char *err) { printf("\n\n ERROR: %s \n\n", err); fflush(NULL); exit(1); }

int random_at_most(int max) {
    unsigned int
    // max <= RAND_MAX < Uint_MAX, so this is okay.
            num_bins = (unsigned int) max + 1,
            num_rand = (unsigned int) RAND_MAX + 1,
            bin_size = num_rand / num_bins,
            defect   = num_rand % num_bins;

    int x;
    do {
        x = random();
    }
        // This is carefully written not to overflow
    while (num_rand - defect <= (unsigned int)x);

    // Truncated division is intentional
    return x/bin_size;
}