#ifndef UTIL_H
#define UTIL_H
#define VERBOSE 100

#include <stdio.h>
#include <stdlib.h>
#include <float.h>

#ifdef linux
#include <time.h>
#endif

/** Prints the specified error an terminates the execution.
*
* @param err Pointer to the string explaining the error.
*/
void print_error(const char *err);

int random_at_most(int max);

#endif // !UTIL_H