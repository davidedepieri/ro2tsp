#ifndef INOUT_H
#define INOUT_H

#include <string.h>
#include "tsp.h"
#include "util.h"

/** Parses the command line and initializes the instance structure
 * with the specified parameters.
 *
 * @param argc Number of strings pointed to by argv
 * @param argv Command line arguments in a vector
 * @param inst Pointer to an instance of the "instance" struct
 */
void parse_command_line(int argc, char** argv, instance *inst);

/** Initializes the instance structure with the input data.
*
* @param inst Pointer to an instance of the "instance" struct
*/
void read_input(instance *inst);

#endif // !INOUT_H
